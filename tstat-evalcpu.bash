#!/usr/bin/bash


qdepth=4
domain="ebi-evalcpu"
datasets=("E-MTAB-4850"
          "E-MTAB-6058"
          "E-GEOD-75367"
          "E-MTAB-6142"
          "E-GEOD-36552"
          "E-GEOD-99795"
          "E-GEOD-110499"
          "E-GEOD-81383"
          "E-GEOD-124858"
          "E-GEOD-109979"
          "E-MTAB-6911"
          "E-MTAB-7381"
          "E-GEOD-111727"
          "E-GEOD-100618"
          "E-GEOD-75688"
          "E-GEOD-86618"
          "E-GEOD-98556"
          "E-GEOD-83139"
          "E-GEOD-70580"
          "E-GEOD-75140"
          "E-GEOD-89232"
          "E-MTAB-7008"
          "E-GEOD-130473"
          "E-MTAB-6819")
          #"E-GEOD-81608"
          #"E-GEOD-76312"
          #"E-GEOD-106540"
          #"E-GEOD-81547"
          #"E-MTAB-5061"
          #"E-GEOD-84465")

# to run a specific dataset only
# datasets=("E-GEOD-76312")
# kinetic_host="10.23.1.172"

datasets=("E-MTAB-6819")
kinetic_host="10.23.90.2"

num_fn_keys=$(( 1 ))


src_dirpath="src/python/resources/sample-data/ebi"
for dname in "${datasets[@]}"; do
    row_count=$(wc -l $(find "${src_dirpath}/${dname}" -type f -name '*.mtx_rows') | sed -E 's/([0-9]+) .*/\1/')
    col_count=$(wc -l $(find "${src_dirpath}/${dname}" -type f -name '*.mtx_cols') | sed -E 's/([0-9]+) .*/\1/')

    result_fpath="results/${domain}.${dname}.${row_count}.${col_count}.tsv"
    # echo -en "${dname}\t" # ${row_count}\t${col_count}"
    # ./build/skyread -h "10.23.89.4" -d "ebi-evalcpu" -k "ebi-evalcpu/${dname};0" -N | grep "Table Excerpt"

    echo "${dname}"
    for iter_ndx in $(seq 1 10); do
        # When running it from the client
        ./build/t.test -h "${kinetic_host}" \
                       -d "${domain}"       \
                       -q "${qdepth}"       \
                       -l "${dname}"        \
                       >> "stats/${dname}.mixed.log"

        # When running it as a pushdown
        # /opt/libkinetic/bin/kctl               \
        #     -h "${kinetic_host}"               \
        #     exec -n ${num_fn_keys} "fn/t.test" \
        #          -h "${kinetic_host}"          \
        #          -d "ebi-evalcpu"              \
        #          -q "${qdepth}"                \
        #          -l "${dname}"                 \
        #     >> "stats/${dname}.log"
    done
done
