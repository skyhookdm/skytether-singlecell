#!/usr/bin/bash

datasets=("E-MTAB-4850"
          "E-MTAB-6058"
          "E-GEOD-75367"
          "E-MTAB-6142"
          "E-GEOD-36552"
          "E-GEOD-99795"
          "E-GEOD-110499"
          "E-GEOD-81383"
          "E-GEOD-124858"
          "E-GEOD-109979"
          "E-MTAB-6911"
          "E-MTAB-7381"
          "E-GEOD-111727"
          "E-GEOD-100618"
          "E-GEOD-75688"
          "E-GEOD-86618"
          "E-GEOD-98556"
          "E-GEOD-83139"
          "E-GEOD-70580"
          "E-GEOD-75140"
          "E-GEOD-89232"
          "E-MTAB-7008"
          "E-GEOD-130473"
          "E-MTAB-6819"
          "E-GEOD-81608"
          "E-GEOD-76312"
          "E-GEOD-106540"
          "E-GEOD-81547"
          "E-MTAB-5061"
          "E-GEOD-84465")

src_host="10.23.1.172"
dest_host="10.23.90.2"

domain_key="ebi-evalcpu"

temp_fpath="key.staged-copy"
for dataset in "${datasets[@]}"; do
    echo "copying: '${dataset}'"

    # Copy the partition slices
    # slice_keys=($(/opt/libkinetic/bin/kctl -h "${src_host}" range -S "${domain_key}/${dataset}" -E "${domain_key}/${dataset}<" | grep ";"))
    # for slice_key in "${slice_keys[@]}"; do
    #     /opt/libkinetic/bin/kctl -h "${src_host}"  get "${slice_key}" > "${temp_fpath}"
    #     /opt/libkinetic/bin/kctl -h "${dest_host}" put -f "${temp_fpath}" "${slice_key}"
    # done

    # Copy just the partition meta
    # /opt/libkinetic/bin/kctl -h "${src_host}"  get "${domain_key}/${dataset}" > "${temp_fpath}"
    # /opt/libkinetic/bin/kctl -h "${dest_host}" put -f "${temp_fpath}" "${domain_key}/${dataset}"
done
