#!/usr/bin/bash

datasets=("E-MTAB-4850"
          "E-MTAB-6058"
          "E-GEOD-75367"
          "E-MTAB-6142"
          "E-GEOD-36552"
          "E-GEOD-99795"
          "E-GEOD-110499"
          "E-GEOD-81383"
          "E-GEOD-124858"
          "E-GEOD-109979"
          "E-MTAB-6911"
          "E-MTAB-7381"
          "E-GEOD-111727"
          "E-GEOD-100618"
          "E-GEOD-75688"
          "E-GEOD-86618"
          "E-GEOD-98556"
          "E-GEOD-83139"
          "E-GEOD-70580"
          "E-GEOD-75140"
          "E-GEOD-89232"
          "E-MTAB-7008"
          "E-GEOD-130473"
          "E-MTAB-6819"
          "E-GEOD-81608"
          "E-GEOD-76312"
          "E-GEOD-106540"
          "E-GEOD-81547"
          "E-MTAB-5061"
          "E-GEOD-84465")

# kinetic_host="10.23.89.4"
kinetic_host="10.23.1.172"

temp_fpath="key.staged-copy"
for dataset in "${datasets[@]}"; do
    echo "modifying: '${dataset}'"
    new_pcount=$(/opt/libkinetic/bin/kctl -h "${kinetic_host}" range -S "ebi-evalcpu/${dataset}" -E "ebi-evalcpu<" | grep ";" | wc -l)

    # dkey="${test_key:0:11}"
    # pkey="${test_key:11}"

    echo "old pcount: "
    ./build/skyread -h "${kinetic_host}" -d "ebi-evalcpu" -m "ebi-evalcpu/${dataset}" | grep "decoded"
    
    # /opt/libkinetic/bin/kctl -h "${kinetic_host}" get "${test_key}" > "${temp_fpath}"
    # ./build/skyupdate        -h "${kinetic_host}" -f "${temp_fpath}" -c "${new_pcount}"
    # /opt/libkinetic/bin/kctl -h "${kinetic_host}" put -f "${temp_fpath}" "${dkey}/${pkey}"

    break
done
