#!/usr/bin/env python

"""
Command-line script for loading an MTX-formatted gene expression matrix into kinetic.
"""


# ------------------------------
# pylint directives

# pylint: disable=invalid-name


# ------------------------------
# Dependencies

import argparse

from scytether import ReadPartitionSlice, ReadPartitionMeta


# ------------------------------
# Module variables

arg_parser = argparse.ArgumentParser(
    description='A simple script to peak at a skytether table partition'
)

arg_parser.add_argument('--keyspace'
    ,dest='partition_keyspace'
    ,type=str
    ,required=False
    ,default='E-CURD-10'
    ,help='Keyspace prefix for table partition to view'
)

# ------------------------------
# Functions

def TestReadMetadata(key_name):
    """ Test view skytether metadata. """

    test_meta       = ReadPartitionMeta(key_name)
    partition_count = int.from_bytes(
         test_meta.metadata['partition_count'.encode('utf-8')]
        ,byteorder='big'
    )

    print(f'partition count -> {partition_count}')


def TestReadData(key_name):
    """Test view skytether data. """
    print(
        ReadPartitionSlice(key_name).slice(0, 10)
                                    .to_pandas()
    )


# ------------------------------
# Main logic

if __name__ == '__main__':
    parsed_args, extra_args = arg_parser.parse_known_args()

    # >> View an excerpt of the partition metadata
    TestReadMetadata(parsed_args.partition_keyspace)

    # >> View a few partition slices
    for slice_ndx in range(3):
        TestReadData(f'{parsed_args.partition_keyspace}.{slice_ndx}')
