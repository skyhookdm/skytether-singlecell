#!/usr/bin/env python

"""
Command-line script for loading an MTX-formatted gene expression matrix into kinetic.
"""


# ------------------------------
# pylint directives

# pylint: disable=invalid-name


# ------------------------------
# Dependencies

import os
import sys
import argparse

from skytether.dataformats import SchemaFromBinary, TableFromBinary


# ------------------------------
# Module variables

metakey_pcount   = 'partition_count'.encode('utf-8')
metakey_clusters = 'clusters'.encode('utf-8')

arg_parser = argparse.ArgumentParser(
    description='A simple script to peak at a skytether table partition'
)

arg_parser.add_argument('--path'
    ,dest='kpath'
    ,type=str
    ,required=False
    ,default='test'
    ,help='Path to file containing skytether metadata or data.'
)

arg_parser.add_argument('--meta-only'
    ,dest='only_meta'
    ,required=False
    ,default=False
    ,action='store_true'
    ,help='flag to only view metadata'
)

arg_parser.add_argument('--data-only'
    ,dest='only_data'
    ,required=False
    ,default=False
    ,action='store_true'
    ,help='flag to only view data slices'
)



# ------------------------------
# Functions

def TestAddClusters(test_pslice):
    """ Super simple function to test cluster lists in partition meta. """

    test_pslice.metadata[metakey_clusters] = 'E-CURD-10.48'.encode('utf-8')

    print(test_pslice)
    return test_pslice


def TestReadMetadata(kv_filepath):
    """ Test view skytether metadata. """

    test_meta = SchemaFromBinary(kv_filepath)

    # modifies test_meta
    TestAddClusters(test_meta)

    # >> partition count
    partition_count = int.from_bytes(test_meta.metadata[metakey_pcount], byteorder='big')
    print(f'partition count -> {partition_count}')

    # >> cluster information
    cluster_labels = test_meta.metadata.get(metakey_clusters, [])
    print(f'cluster labels  -> {cluster_labels}')

    return test_meta


def TestReadData(kv_filepath):
    """Test view skytether data. """
    test_data = TableFromBinary(kv_filepath)
    print(test_data.slice(0, 10).to_pandas())

    return test_data


# ------------------------------
# Main logic

if __name__ == '__main__':
    parsed_args, extra_args = arg_parser.parse_known_args()

    # >> View an excerpt of the partition metadata
    if not parsed_args.only_data:
        path_to_meta = f'{parsed_args.kpath}.kmeta'
        if not os.path.isfile(f'{path_to_meta}'):
            sys.exit(f'Invalid file path: "{path_to_meta}"')

        TestReadMetadata(path_to_meta)
        # TestAddClusters(path_to_meta)

    # >> View a few partition slices
    if not parsed_args.only_meta:
        for slice_ndx in range(3):
            path_to_slice = f'{parsed_args.kpath}.slice.{slice_ndx}'

            if not os.path.isfile(path_to_slice):
                sys.exit(f'Invalid file path: "{path_to_slice}"')

            TestReadData(path_to_slice)
