/**
 * A command-line tool that takes parses IPC-formatted arrow data from:
 * - A local file path (absolute or relative)
 * - A key name (will query a kinetic drive)
 */

#include <unistd.h>

#include "../headers/skykinetic.hpp"
#include "../headers/skyhookfs.hpp"
#include "../headers/bio.hpp"

using Skytether::SetPartitionCount;

const char *option_template = "h:f:k:m:c:";

int PrintHelp() {
    std::cout << "skyupdate <[-f path-to-file | -k key-name | -m metakey-name]>"
              << " [-c <new partition count>]"
              << std::endl
    ;

    return 1;
}


int main(int argc, char **argv) {
    bool        is_src_set    = false;
    bool        is_domain_set = false;
    bool        is_metakey    = false;
    bool        only_info     = false;
    int64_t     num_rows      = 10;
    uint8_t     stripe_size   = 1;
    size_t      new_pcount    = 0;

    std::string host_ip { "10.23.89.5" };
    std::string domain_name;

    KineticConn *kconn = nullptr;

    unique_ptr<KBuffer> slice_buffer;
    Result<shared_ptr<Table>> table_result = Status::Invalid("No query made yet");

    char parsed_opt;
    while ((parsed_opt = (char) getopt(argc, argv, option_template)) != (char) -1) {
        switch (parsed_opt) {
            case 'h': {
                host_ip = std::string { optarg };
                std::cout << "Changing kinetic IP to: " << host_ip << std::endl;
                break;
            }

            case 'f': {
                if (is_src_set) { return PrintHelp(); }
                is_src_set = true;

                fs::path path_to_arrow { local_file_protocol + fs::absolute(optarg).string() };
                sky_debug_printf("Parsing file: '%s'\n", path_to_arrow.string().data());

                // Create a RecordBatchStreamReader for the given `path_to_arrow`
                table_result = ReadIPCFile(path_to_arrow.string());

                break;
            }

            case 'd': {
                is_domain_set = true;
                domain_name   = string { optarg };
                kconn         = new KineticConn(domain_name);
                break;
            }

            // set flag, then fall through to getting a reader for the metakey
            case 'm': {
                is_metakey = true;
                [[fallthrough]];
            }

            case 'k': {
                // if (is_src_set or not is_domain_set) { return PrintHelp(); }
                if (is_src_set) { return PrintHelp(); }
                is_src_set = true;

                // std::string table_key { "/" + string { optarg } };
                std::string table_key { optarg };

                sky_debug_printf("Connecting to kinetic [%s]\n", host_ip.data());
                kconn->Connect(host_ip.data());

                if (kconn->IsConnected()) {
                    sky_debug_printf("Reading key: '%s'\n", table_key.data());
                    auto get_result = kconn->GetSliceData(table_key, stripe_size);
                    if (not get_result.ok()) {
                        table_result = Result<shared_ptr<Table>>(
                            Status::Invalid("Could not get data for partition slice")
                        );
                    }

                    slice_buffer = std::move(get_result.ValueOrDie());
                    auto reader_result = slice_buffer->NewRecordBatchReader();
                    if (not reader_result.ok()) {
                        slice_buffer.reset();
                        table_result = Result<shared_ptr<Table>>(
                            Status::Invalid("Unable to construct reader for slice buffer")
                        );
                    }

                    else {
                        auto reader  = reader_result.ValueOrDie();
                        table_result = Table::FromRecordBatchReader(reader.get());
                    }
                }
                else {
                    table_result = Result<shared_ptr<Table>>(
                        Status::Invalid("Could not connect to kinetic drive")
                    );
                }

                break;
            }

            case 'c': {
                new_pcount = (size_t) std::stoll(optarg);
                break;
            }

            case 'v': {
                break;
            }

            default: {
                table_result = Result<shared_ptr<Table>>(Status::Invalid("Invalid argument"));

                break;
            }
        }
    }

    if (not table_result.ok()) {
        std::cerr << "Error: '" << table_result.status() << "'" << std::endl;
        return 1;
    }

    auto table_data = table_result.ValueOrDie();
    auto table_meta = table_data->schema()->metadata()->Copy();
    PrintSchemaMetadata(table_meta);

    auto set_status = SetPartitionCount(table_meta, new_pcount);
    if (not set_status.ok()) {
        std::cerr << "Error setting new partition count" << std::endl;
        return 1;
    }

    PrintSchemaMetadata(table_meta);

    table_data = table_data->ReplaceSchemaMetadata(table_meta);
    auto buffer_result = WriteTableToBuffer(table_data);
    if (buffer_result.ok()) {
        auto put_status = kconn->PutKey("updated.test", *buffer_result);

        if (not put_status.ok()) {
            std::cerr << "Failed to put key" << std::endl;
        }
    }
    else {
        std::cerr << "Failed to write table to memory buffer" << std::endl;
    }

    if (kconn) { kconn->PrintStats(); }

    return 0;
}

