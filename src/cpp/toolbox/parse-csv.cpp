#include "../headers/bio.hpp"

int main(int argc, char **argv) {
    if (argc != 2) {
        printf("Please provide just the path to a file to parse\n");
        return 1;
    }

    sky_debug_printf("Parsing file: '%s'\n", argv[1]);

    // Home-grown CSV parser
    std::vector<StrVec> file_data = parse_tsv_file(argv[1]);
    
    sky_debug_block(print_record_attrs(attribute_names);)
    sky_debug_block(print_filedata(file_data);)

    return 0;
}
