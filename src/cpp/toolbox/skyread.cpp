/**
 * A command-line tool that takes parses IPC-formatted arrow data from:
 * - A local file path (absolute or relative)
 * - A key name (will query a kinetic drive)
 */

#include <unistd.h>

#include "../headers/skykinetic.hpp"
#include "../headers/skyhookfs.hpp"
#include "../headers/bio.hpp"


const char *option_template = "h:f:k:m:n:s:Nid:";

int PrintHelp() {
    std::cout << "skyread <[-f path-to-file | -k key-name | -m metakey-name]>"
              << " [-d domain name]"
              << " [-n row-count]"
              << " [-N]"
              << " [-i]"
              << " [-s <stripe size> (default: 1)]"
              << std::endl
    ;


    std::cout << "NOTE:"
              << std::endl
              << "'-d' must be provided before '-k' or '-m' and specifies the key prefix"
              << std::endl
              << "'-s' only affects '-k' and must be provided first"
              << std::endl
    ;

    return 1;
}


int main(int argc, char **argv) {
    bool        is_src_set    = false;
    bool        is_domain_set = false;
    bool        is_metakey    = false;
    bool        only_info     = false;
    int64_t     num_rows      = 10;
    uint8_t     stripe_size   = 1;

    std::string host_ip { "10.23.89.5" };
    std::string domain_name;

    KineticConn *kconn = nullptr;

    unique_ptr<KBuffer> slice_buffer;
    Result<shared_ptr<Table>> table_result = Status::Invalid("No query made yet");

    char parsed_opt;
    while ((parsed_opt = (char) getopt(argc, argv, option_template)) != (char) -1) {
        switch (parsed_opt) {
            case 'h': {
                host_ip = std::string { optarg };
                std::cout << "Changing kinetic IP to: " << host_ip << std::endl;
                break;
            }

            case 'n': {
                num_rows = std::stoll(optarg);
                sky_debug_printf("Setting excerpt size to: %ld\n", num_rows);
                break;
            }

            case 'N': {
                num_rows = -1;
                sky_debug_printf("Removing excerpt size\n");
                break;
            }

            case 'f': {
                if (is_src_set) { return PrintHelp(); }
                is_src_set = true;

                fs::path path_to_arrow { local_file_protocol + fs::absolute(optarg).string() };
                sky_debug_printf("Parsing file: '%s'\n", path_to_arrow.string().data());

                // Create a RecordBatchStreamReader for the given `path_to_arrow`
                table_result = ReadIPCFile(path_to_arrow.string());

                break;
            }

            case 'd': {
                is_domain_set = true;
                domain_name   = string { optarg };
                kconn         = new KineticConn(domain_name);
                break;
            }

            // set flag, then fall through to getting a reader for the metakey
            case 'm': {
                is_metakey = true;
                [[fallthrough]];
            }

            case 'k': {
                if (is_src_set or not is_domain_set) { return PrintHelp(); }
                is_src_set = true;

                // std::string table_key { "/" + string { optarg } };
                std::string table_key { optarg };

                sky_debug_printf("Connecting to kinetic [%s]\n", host_ip.data());
                kconn->Connect(host_ip.data());

                if (kconn->IsConnected()) {
                    sky_debug_printf("Reading key: '%s'\n", table_key.data());
                    auto get_result = kconn->GetSliceData(table_key, stripe_size);
                    if (not get_result.ok()) {
                        table_result = Result<shared_ptr<Table>>(
                            Status::Invalid("Could not get data for partition slice")
                        );
                    }

                    slice_buffer = std::move(get_result.ValueOrDie());
                    auto reader_result = slice_buffer->NewRecordBatchReader();
                    if (not reader_result.ok()) {
                        slice_buffer.reset();
                        table_result = Result<shared_ptr<Table>>(
                            Status::Invalid("Unable to construct reader for slice buffer")
                        );
                    }

                    else {
                        auto reader  = reader_result.ValueOrDie();
                        table_result = Table::FromRecordBatchReader(reader.get());
                    }
                }
                else {
                    table_result = Result<shared_ptr<Table>>(
                        Status::Invalid("Could not connect to kinetic drive")
                    );
                }

                break;
            }

            case 's': {
                int raw_stripesize = std::stoi(optarg);

                if (raw_stripesize < 0 or raw_stripesize > 255) {
                    std::cerr << "Invalid stripe size; must be in the range [0, 255]"
                              << std::endl
                    ;

                    return 1;
                }

                stripe_size = (uint8_t) raw_stripesize;
                break;
            }

            case 'i': {
                only_info = true;
                break;
            }

            default: {
                table_result = Result<shared_ptr<Table>>(Status::Invalid("Invalid argument"));

                break;
            }
        }
    }

    if (not table_result.ok()) {
        std::cerr << "Error: '" << table_result.status() << "'" << std::endl;
        return 1;
    }

    if (not only_info) {
        auto table_data = table_result.ValueOrDie();
        if (is_metakey) { PrintSchema(table_data->schema(), 0, num_rows); }
        else            { PrintTable(table_data, 0, num_rows);            }
    }

    if (kconn) { kconn->PrintStats(); }

    return 0;
}

