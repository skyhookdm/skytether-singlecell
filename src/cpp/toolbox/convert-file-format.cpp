#include "../headers/skyhookfs.hpp"
#include "../headers/bio.hpp"
#include "../headers/datatypes.hpp"


#if USE_BOOSTFS == 1
	namespace fs = boost::filesystem;
#else
	namespace fs = std::filesystem;
#endif


int main(int argc, char **argv) {
    if (argc < 2 or argc > 5) {
        printf("convert-file-format <path-to-inputfile> [<path-to-output-basepath>] [batch-size]\n");
        return 1;
    }

    fs::path input_uripath { local_file_protocol + fs::absolute(argv[1]).string() };
    sky_debug_printf("Parsing file: '%s'\n", input_uripath.string().data());

    fs::path path_to_output_base;
    if (argc >= 3) {
        path_to_output_base = local_file_protocol + fs::absolute(argv[2]).string();
    }
    else {
        path_to_output_base = local_file_protocol + "converted-file";
    }

    sky_debug_printf("Output files will be: '%s' + '<index>.arrow'\n", path_to_output_base.string().data());

    // Initialize and configure an instance of Arrow's CSV parser
    sky_debug_printf("Assuming gene expression file for now...\n");
    // Result<shared_ptr<TableReader>> csv_reader = ReaderForGeneAnnotations(input_uripath);
    auto csv_reader = ReaderForGeneExpressions(input_uripath.string());
    if (not csv_reader.ok()) {
        std::cout << "Unable to create reader: '" << input_uripath << "'"
                  << std::endl
        ;

        return 1;
    }

    // If the CSV reader was successfully configured, call `Read()`
    auto read_status = csv_reader.ValueOrDie()->Read();
    if (not read_status.ok()) {
        std::cout << "Unable to parse file" << std::endl;
        return 1;
    }

    auto gene_annotations = read_status.ValueOrDie();
    sky_debug_block(PrintTable(gene_annotations);)

    // batch_size appears to be number of rows in the batch
    // (see: https://github.com/apache/arrow/blob/apache-arrow-3.0.0/cpp/src/arrow/table.cc#L601)
    int64_t batch_size = 2048;
    if (argc == 4) { batch_size = std::stoi(argv[3]); }

    arrow::Status write_status = WriteAsRecordBatches(
         gene_annotations
        ,path_to_output_base.string()
        ,batch_size
    );

    return 0;
}

