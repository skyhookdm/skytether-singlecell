/**
 * A command-line tool that takes a local file path to a TSV file and loads it into a kinetic
 */

#include <unistd.h>

#include "../headers/skytether.hpp"
#include "../headers/skykinetic.hpp"
#include "../headers/skyhookfs.hpp"
#include "../headers/bio.hpp"


const char *option_template = "f:d:b:k:";

int PrintHelp() {
    std::cout << "skyload"
              << " -f path-to-file"
              << " -k partition-key"
              << " [-d domain-key]"
              << " [-b byte-batch-size]"
              << std::endl
    ;

    return 1;
}


int main(int argc, char **argv) {
    // Default max kinetic key-value size (1 MiB in bytes)
    int64_t     byte_batch_size { 1024 * 1024 };
    bool        isset_path      { false       };
    bool        isset_key       { false       };

    fs::path    path_to_inputfile;
    std::string domain_key    { "public" };
    std::string partition_key { "skyload:default" };

    char parsed_opt;
    while ((parsed_opt = (char) getopt(argc, argv, option_template)) != (char) -1) {
        switch (parsed_opt) {
            case 'f': {
                isset_path        = true;
                path_to_inputfile = local_file_protocol + fs::absolute(optarg).string();
                sky_debug_printf("Loading file: '%s'\n", path_to_inputfile.string().data());

                break;
            }

            case 'd': {
                domain_key = std::string(optarg);
                sky_debug_printf("Loading into domain: '%s'\n", domain_key.data());

                break;
            }

            case 'k': {
                isset_key     = true;
                partition_key = std::string(optarg);
                sky_debug_printf("Loading into partition: '%s'\n", partition_key.data());
                break;
            }

            case 'b': {
                byte_batch_size = std::stoll(optarg);
                sky_debug_printf("Loading into batch size: %ld (bytes)\n", byte_batch_size);

                break;
            }

            default: { break; }
        }
    }

    if (not isset_path || not isset_key) {
        std::cerr << "file path and key name are required" << std::endl;
        return 1;
    }

    // Initialize and configure an instance of Arrow's CSV parser
    sky_debug_printf("Assuming gene expression file for now...\n");

    // Result<shared_ptr<TableReader>> csv_reader = ReaderForGeneAnnotations(path_to_inputfile);
    auto csv_reader = ReaderForGeneExpressions(path_to_inputfile.string());
    if (not csv_reader.ok()) {
        std::cout << "Unable to create reader: '" << path_to_inputfile << "'"
                  << std::endl
        ;

        return 1;
    }

    // If the CSV reader was successfully configured, call `Read()`
    auto read_result = csv_reader.ValueOrDie()->Read();
    if (not read_result.ok()) {
        std::cout << "Unable to parse file" << std::endl;
        return 1;
    }

    // A convenience function to peek at the file data
    auto table_data = read_result.ValueOrDie();
    sky_debug_block(PrintTable(table_data, 0, 10);)

    KineticConn kconn { domain_key };
    kconn.Connect();
    if (not kconn.IsConnected()) {
        std::cerr << "Unable to connect to kinetic server" << std::endl;
        return 1;
    }

    sky_debug_printf("Connection ID: %d\n", kconn.conn_id);
    auto put_status = kconn.PutPartition(partition_key, table_data, byte_batch_size);

    kconn.Disconnect();

    if (not put_status.ok()) {
        std::cerr << "Failed to bulk load source data" << std::endl;
        return 1;
    }

    return 0;
}

