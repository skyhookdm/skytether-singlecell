/**
 * Code for tokenizing a SQL string, to keep the parser at a higher-level abstraction
 */


// ------------------------------
// Dependencies

#include "../headers/sql.hpp"


// ------------------------------
// Global variables

// Reserved tokens
const char        token_sep     (' ');
const char        id_sep        (',');

const std::string keyword_select("SELECT");
const std::string keyword_from  ("FROM"  );
const std::string keyword_where ("WHERE" );

const std::string op_eq         ("="     );
const std::string op_le         ("<="    );
const std::string op_lt         ("<"     );
const std::string op_ge         (">="    );
const std::string op_gt         (">"     );


// ------------------------------
// Classes and Types

typedef ParseResult (*)(char *unparsed) ParserFnType;

struct ParseResult {
    std::string *token;
    char        *unparsed;
};


// ------------------------------
// Functions

// >> Recognizers
bool IsAlpha(char unparsed) {
    return (
           (unparsed >= 'a' and unparsed <= 'z')
        or (unparsed >= 'A' and unparsed <= 'Z')
    );
}

bool IsNumeric(char unparsed) {
    return unparsed >= '0' and unparsed <= '9';
}

bool IsAlphaNumeric(char unparsed) {
    return IsNumeric(unparsed) or IsAlpha(unparsed);
}


// >> Functions to skip unnecessary parts
char* NextTokenStart(char *unparsed) {
    while (unparsed and *unparsed == token_sep) { unparsed++; }

    return unparsed;
}

char* NextIdStart(char *unparsed) {
    unparsed = SkipWhitespace(unparsed);

    if (unparsed and *unparsed == id_sep) { return unparsed + 1; }
    return unparsed;
}

char* ExpectToken(std::string expected, char *unparsed) {
    size_t token_pos = 0;

    if (unparsed == nullptr) { return unparsed; }

    unparsed = NextTokenStart(unparsed);

    while (token_pos < expected.length()) {
        if (unparsed[token_pos] == expected.data()[token_pos]) { token_pos++; }
    }

    if (token_pos == expected.length()) { unparsed + token_pos; }
    return nullptr;
}


// >> Parser Functions
ParseResult NextToken(ParserFnType, char *unparsed) {
    char *token_start = NextTokenStart(unparsed);

    if (token_start == nullptr) { return ParseResult(nullptr, unparsed); }
    return (ParserFnType)(unparsed);
}


ParseResult ParseId(char *unparsed) {
    size_t token_len = 0;

    if (IsAlpha(unparsed[0])) { token_len = 1; }

    while (unparsed[token_len]) {
        if (not IsAlphaNumeric(unparsed[token_len])) { break; }

        token_len++;
    }

    return ParseResult(std::string(unparsed, token_len), unparsed + token_len);
}
