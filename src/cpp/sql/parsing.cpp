/**
 * This file contains code for parsing SQL strings into a query tree.
 */


// ------------------------------
// Dependencies
#include "../headers/skytether.hpp"
#include "../headers/sql.hpp"

using arrow::compute::equal;
using arrow::compute::less_equal;
using arrow::compute::less;
using arrow::compute::greater_equal;
using arrow::compute::greater;


// ------------------------------
// Constants

const char token_sep(' ');
const char id_sep(',');

KBuffer keyword_select((uint8_t *) "SELECT", 6);
KBuffer keyword_from  ((uint8_t *) "FROM"  , 4);
KBuffer keyword_where ((uint8_t *) "WHERE" , 5);

KBuffer op_eq((uint8_t *) "=" , 1);
KBuffer op_le((uint8_t *) "<=", 2);
KBuffer op_lt((uint8_t *) "<" , 1);
KBuffer op_ge((uint8_t *) ">=", 2);
KBuffer op_gt((uint8_t *) ">" , 1);


// ------------------------------
// Functions

char* consume_whitespace(char *input_str) {
    while (input_str and *input_str == token_sep) { input_str++; }

    return input_str;
}

char* consume_idsep(char *input_str) {
    // separator can have any amount of whitespace before
    input_str = consume_whitespace(input_str);

    if (input_str and *input_str == id_sep) { return input_str + 1; }

    return input_str;
}

size_t traverse_token(char *input_str) {
    if (input_str == nullptr) { return 0; }

    size_t token_size = 0;
    while (input_str[token_size]) {
        if (input_str[token_size] == token_sep or input_str[token_size] == id_sep) { break; }

        token_size++;
    }

    return token_size;
}

char* consume_token(char *input_str, KBuffer *output_token) {
    sky_debug_printf("Consuming token\n");

    char   *token_start = consume_whitespace(input_str);
    size_t  token_size  = traverse_token(token_start);

    output_token->base = (uint8_t *) token_start;
    output_token->len  = token_size;

    return token_start + token_size;
}

char* expect_token(char *input_str, KBuffer *exp_token) {
    KBuffer parsed_token;

    char *unparsed_str = consume_token(input_str, &parsed_token);

    if (exp_token->Matches(&parsed_token)) { return unparsed_str; }
    return nullptr;
}


// TODO: in progress
// Make parsing expressions a function so that we can more easily parse multiple predicates
/*
arrow::Result<Expression>
parse_expression(char *input_str, char **unparsed) {
    KBuffer predicate_field;
    KBuffer predicate_op;
    KBuffer predicate_val;

    char *parsed = input_str;
    *unparsed    = consume_token(parsed, &predicate_field);
    if (not predicate_field.len) {
        return arrow::Result<Expression>(
            arrow::Status::Invalid("Unable to parse ID from predicate")
        );
    }

    parsed   = *unparsed;
    *unparsed = consume_token(parsed, &predicate_op);
    if (not predicate_op.len) {
        return arrow::Result<Expression>(
            arrow::Status::Invalid("Unable to parse operator from predicate")
        );
    }

    parsed    = *unparsed;
    *unparsed = consume_token(parsed, &predicate_val);
    if (not predicate_val.len) {
        return arrow::Result<Expression>(
            arrow::Status::Invalid("Unable to parse literal from predicate")
        );
    }
}
*/


arrow::Result<StrVec>
parse_clause_select(char *query_str, char **unparsed) {
    sky_debug_printf("Parsing clause: SELECT\n");

    StrVec  parsed_ids;
    KBuffer    parsed_token;

    // make sure this clause starts with `SELECT` keyword
    char *parsed = query_str;
    *unparsed    = expect_token(parsed, &keyword_select);
    if (*unparsed == nullptr) {
        return arrow::Result<StrVec>(
            arrow::Status::Invalid("Missing keyword 'SELECT'")
        );
    }

    // inch `parsed` forward
    parsed    = *unparsed;
    *unparsed = consume_token(parsed, &parsed_token);

    while (parsed_token.len) {
        if (keyword_from.Matches(&parsed_token)) {
            *unparsed = parsed;
            break;
        }

        parsed_ids.push_back(parsed_token.ToString());

        parsed    = consume_idsep(*unparsed);
        *unparsed = consume_token(parsed, &parsed_token);
    } 

    if (parsed_ids.empty()) {
        if (consume_whitespace(parsed)[0] == id_sep) {
            return arrow::Result<StrVec>(
                arrow::Status::Invalid("Found separator; expected attribute")
            );
        }

        return arrow::Result<StrVec>(
            arrow::Status::Invalid("No attributes in projection")
        );
    }

    return arrow::Result<StrVec>(parsed_ids);
}

/**
 * Currently hard-coded to only parse one source table
 */
arrow::Result<std::string>
parse_clause_from(char *query_str, char **unparsed) {
    sky_debug_printf("Parsing clause: FROM\n");

    KBuffer parsed_token;

    char *parsed = query_str;
    *unparsed    = expect_token(parsed, &keyword_from);
    if (*unparsed == nullptr) {
        return arrow::Result<std::string>(
            arrow::Status::Invalid("Missing keyword 'FROM'")
        );
    }

    parsed    = *unparsed;
    *unparsed = consume_token(parsed, &parsed_token);
    if (not parsed_token.len) {
        return arrow::Result<std::string>(
            arrow::Status::Invalid("[FROM] Parse failed")
        );
    }

    return arrow::Result<std::string>(parsed_token.ToString());
}

arrow::Result<Expression>
parse_clause_where(char *query_str, char **unparsed) {
    sky_debug_printf("Parsing clause: WHERE\n");

    char *parsed = query_str;
    *unparsed    = expect_token(parsed, &keyword_where);
    if (*unparsed == nullptr) {
        return arrow::Result<Expression>(
            arrow::Status::Invalid("Missing keyword 'WHERE'")
        );
    }

    KBuffer predicate_field;
    KBuffer predicate_op;
    KBuffer predicate_val;

    parsed    = *unparsed;
    *unparsed = consume_token(parsed, &predicate_field);
    if (not predicate_field.len) {
        return arrow::Result<Expression>(
            arrow::Status::Invalid("Unable to parse ID from predicate")
        );
    }

    parsed   = *unparsed;
    *unparsed = consume_token(parsed, &predicate_op);
    if (not predicate_op.len) {
        return arrow::Result<Expression>(
            arrow::Status::Invalid("Unable to parse operator from predicate")
        );
    }

    parsed    = *unparsed;
    *unparsed = consume_token(parsed, &predicate_val);
    if (not predicate_val.len) {
        return arrow::Result<Expression>(
            arrow::Status::Invalid("Unable to parse literal from predicate")
        );
    }

    // Otherwise, return success
    auto pred_attr    = arrow::compute::field_ref(predicate_field.ToString());
    auto pred_literal = arrow::compute::literal(predicate_val.ToString());

    if (op_eq.Matches(&predicate_op)) {
        return arrow::Result<Expression>(equal(pred_attr, pred_literal));
    }
    else if (op_le.Matches(&predicate_op)) {
        return arrow::Result<Expression>(less_equal(pred_attr, pred_literal));
    }
    else if (op_lt.Matches(&predicate_op)) {
        return arrow::Result<Expression>(less(pred_attr, pred_literal));
    }
    else if (op_ge.Matches(&predicate_op)) {
        return arrow::Result<Expression>(greater_equal(pred_attr, pred_literal));
    }
    else if (op_gt.Matches(&predicate_op)) {
        return arrow::Result<Expression>(greater(pred_attr, pred_literal));
    }

    // Default to invalid
    return arrow::Result<Expression>(
        arrow::Status::NotImplemented("[WHERE] unable to parse predicate")
    );
}

arrow::Result<QueryPlan>
parse_query(const char *query_str) {
    char *select_clause_start = (char *) query_str;

    char *from_clause_start  = NULL;
    char *where_clause_start = NULL;
    char *unparsed           = NULL;

    std::string source_table;
    StrVec      projection_attrs;
    Expression  where_predicate;

    ARROW_ASSIGN_OR_RAISE(
         projection_attrs
        ,parse_clause_select(select_clause_start, &from_clause_start)
    );

    if (from_clause_start == nullptr) {
        return arrow::Result<QueryPlan>(
            arrow::Status::Invalid("Could not find clause 'FROM'")
        );
    }

    ARROW_ASSIGN_OR_RAISE(
         source_table
        ,parse_clause_from(from_clause_start, &where_clause_start)
    );

    where_clause_start = consume_whitespace(where_clause_start);
    if (where_clause_start and *where_clause_start) {
        ARROW_ASSIGN_OR_RAISE(
             where_predicate
            ,parse_clause_where(where_clause_start, &unparsed)
        );

        return arrow::Result<QueryPlan>(
            QueryPlan(source_table, projection_attrs, where_predicate)
        );
    }

    return arrow::Result<QueryPlan>(QueryPlan(source_table, projection_attrs));
}
