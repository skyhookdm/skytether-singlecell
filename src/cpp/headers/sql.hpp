/**
 * Header for SQL parsing code that does not need to be visible to the rest of the library.
 */

#ifndef __SKYHOOKSQL_HPP
#define __SKYHOOKSQL_HPP


// ------------------------------
// Dependencies
#include "skytether.hpp"

#include <string.h>
#include <map>


// ------------------------------
// Classes and structs

// ..............................
// >> Type aliases

typedef std::vector<Expression>           ExprVector;
typedef std::map<std::string, ExprVector> JoinMapping;


// ..............................
// >> Using declarations for convenience

// |> Classes
using arrow::compute::Expression;

// |> terminals
using arrow::compute::field_ref; // reference to a "field" (column)
using arrow::compute::literal;   // wrapper for a literal value

// |> relational operators
using arrow::compute::project;  // (ExprVector, StrVec)

// |> boolean operators (unary):
using arrow::compute::is_null;  // (Expression)
using arrow::compute::is_valid; // (Expression)
using arrow::compute::not_;     // (Expression)

// |> boolean operators (binary or vector):
//         [`operator&&`] (Expression, Expression)
//      or (const std::vector<Expression>&)
using arrow::compute::and_; 

//         [`operator||`] (Expression, Expression)
//      or (const std::vector<Expression>&)
using arrow::compute::or_; 

// |> equality operators (Expression, Expression):
using arrow::compute::equal; 
using arrow::compute::not_equal; 
using arrow::compute::less; 
using arrow::compute::less_equal; 
using arrow::compute::greater; 
using arrow::compute::greater_equal; 

// |> Serialization
using arrow::compute::Serialize;   // returns Result<std::shared_ptr<Buffer>>
using arrow::compute::Deserialize; // returns Result<Expression>


// ------------------------------
// Classes and Structs

// ..............................
// >> Query Types

struct OpProjection {
    ExprVector exprs;
    StrVec     names;

    OpProjection(ExprVector &new_exprs, StrVec &new_names) {
        exprs = new_exprs;
        names = new_names;
    }
};

struct OpJoin {
    StrVec      rels;
    JoinMapping preds;

    OpJoin(StrVec &relations, JoinMapping &join_preds) {
        rels  = relations;
        preds = join_preds;
    }
};

struct OpSelection {
    Expression expr;
};


// ------------------------------
// Functions

char* parse_identifier(char *query_str);
char* consume_whitespace(char *input_str);

arrow::Result<StrVec>      parse_clause_select(char *query_str, char **unparsed);
arrow::Result<std::string> parse_clause_from  (char *query_str, char **unparsed);
arrow::Result<Expression>  parse_clause_where (char *query_str, char **unparsed);


#endif //__SKYHOOKSQL_HPP
