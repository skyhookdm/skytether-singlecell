/**
 * Functions and methods for reading data from various sources, or for returning reader objects.
 */

#ifndef __BIO_HPP
#define __BIO_HPP

// ------------------------------
// Dependencies


// Main header
#include "skytether.hpp"

/* Additional C++ includes */
#include <memory>
#include <unordered_map>
#include <fstream>

/* Third party dependencies */
#include <arrow/csv/api.h>


// ------------------------------
// Globals

// >> Type aliases for readability

using arrow::csv::TableReader;

//    |> classes and interfaces
using  CSVReadOpts    = arrow::csv::ReadOptions;
using  CSVParseOpts   = arrow::csv::ParseOptions;
using  CSVConvertOpts = arrow::csv::ConvertOptions;

//     |> collection types
using AttributeTypeMap = std::unordered_map<std::string, shared_ptr<DataType>>;


// >> Static variables (for parsers)
static StrVec annot_attribute_names {
    "featurekey"  ,
    "featurename" ,
    "featuretype" ,
    "chromosome"  ,
    "featurestart",
    "featureend"  ,
    "isgene"
};

static StrVec expr_attribute_names {
    "gene_name",
    "cell_id"  ,
    "expr_val"
};

static AttributeTypeMap annot_attribute_types = {
    { "featurekey"  , arrow::utf8()    },
    { "featurename" , arrow::utf8()    },
    { "featuretype" , arrow::utf8()    },
    { "chromosome"  , arrow::utf8()    },
    { "featurestart", arrow::uint32()  },
    { "featureend"  , arrow::uint32()  },
    { "isgene"      , arrow::boolean() }
};


static AttributeTypeMap expr_attribute_types = {
    { "gene_name", arrow::utf8()    },
    { "cell_id"  , arrow::utf8()    },
    // { "expr_val" , arrow::uint16()  }
    { "expr_val" , arrow::float64()  }
};


// ------------------------------
// API

// >> for parsers of input files (from biology community)
arrow::Result<shared_ptr<TableReader>> ReaderForGeneAnnotations(const std::string &uri_path);
arrow::Result<shared_ptr<TableReader>> ReaderForGeneExpressions(const std::string &uri_path);

#endif // __BIO_HPP
