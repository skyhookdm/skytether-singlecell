#include "../headers/bio.hpp"


CSVReadOpts
readopts_for_annot() {
    CSVReadOpts parser_readopts = CSVReadOpts::Defaults();

    // customize options for this particular file for now
    parser_readopts.use_threads               = false;
    parser_readopts.skip_rows                 = 1;
    parser_readopts.column_names              = annot_attribute_names;
    parser_readopts.autogenerate_column_names = false;

    return parser_readopts;
}


CSVReadOpts
readopts_for_expression() {
    CSVReadOpts parser_readopts = CSVReadOpts::Defaults();

    // customize options for this particular file for now
    parser_readopts.use_threads               = false;
    parser_readopts.skip_rows                 = 0;
    parser_readopts.column_names              = expr_attribute_names;
    parser_readopts.autogenerate_column_names = false;
    parser_readopts.block_size                = 1 << 28;

    return parser_readopts;
}


CSVParseOpts
parseopts_for_annot() {
    CSVParseOpts parser_parseopts = CSVParseOpts::Defaults();

    // customize options for this particular file for now
    parser_parseopts.delimiter  = '\t';
    parser_parseopts.quoting    = false;
    parser_parseopts.quote_char = '"';

    return parser_parseopts;
}


CSVConvertOpts
convertopts_for_annot() {
    CSVConvertOpts parser_convertopts = CSVConvertOpts::Defaults();

    // customize convert options
    parser_convertopts.column_types        = annot_attribute_types;
    parser_convertopts.null_values         = StrVec { ""      };
    parser_convertopts.true_values         = StrVec { "True"  };
    parser_convertopts.false_values        = StrVec { "False" };
    parser_convertopts.strings_can_be_null = true;

    return parser_convertopts;
}


CSVConvertOpts
convertopts_for_expression() {
    CSVConvertOpts parser_convertopts = CSVConvertOpts::Defaults();

    // customize convert options
    parser_convertopts.column_types        = expr_attribute_types;
    parser_convertopts.null_values         = StrVec { ""      };
    parser_convertopts.true_values         = StrVec { "True"  };
    parser_convertopts.false_values        = StrVec { "False" };
    parser_convertopts.strings_can_be_null = true;

    return parser_convertopts;
}


/**
 * Factory for creating a metadata file reader using Arrow's CSV implementation of TableReader
 */
Result<shared_ptr<TableReader>>
ReaderForGeneAnnotations(const std::string &uri_path) {
    std::string path_to_file;

    // get a `FileSystem` instance (local fs scheme is "file://")
    ARROW_ASSIGN_OR_RAISE(auto localfs, arrow::fs::FileSystemFromUri(uri_path, &path_to_file));

    // create a handle for the file (expecting a RandomAccessFile type)
    ARROW_ASSIGN_OR_RAISE(auto file_to_parse, localfs->OpenInputFile(path_to_file));

    // Make a csv-based TableReader with the given input and options
    return arrow::csv::TableReader::Make(
        arrow::io::default_io_context(),
        file_to_parse              ,
        readopts_for_annot()       ,
        parseopts_for_annot()      ,
        convertopts_for_annot()
    );
}


/**
 * Factory for creating a metadata file reader using Arrow's CSV implementation of TableReader
 */
Result<shared_ptr<TableReader>>
ReaderForGeneExpressions(const std::string &uri_path) {
    std::string path_to_file;

    // get a `FileSystem` instance (local fs scheme is "file://")
    ARROW_ASSIGN_OR_RAISE(auto localfs, arrow::fs::FileSystemFromUri(uri_path, &path_to_file));

    // create a handle for the file (expecting a RandomAccessFile type)
    ARROW_ASSIGN_OR_RAISE(auto file_to_parse, localfs->OpenInputFile(path_to_file));

    // Make a csv-based TableReader with the given input and options
    return arrow::csv::TableReader::Make(
        arrow::io::default_io_context() ,
        file_to_parse               ,
        readopts_for_expression()   ,
        parseopts_for_annot()       ,
        convertopts_for_expression()
    );
}
