import os
import sys

from skytether.dataformats import SchemaFromBinary

if len(sys.argv) > 1 and os.path.isfile(sys.argv[1]):
    with open(sys.argv[1], 'rb') as in_handle:
        arrow_schema = SchemaFromBinary(in_handle.read())

print(arrow_schema.to_string(show_schema_metadata=False))
partition_count = arrow_schema.metadata['partition_count'.encode()]
print('partition_count -> {}'.format(
    int.from_bytes(partition_count, byteorder="big")
))
