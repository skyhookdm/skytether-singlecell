import sys

import numpy
import pyarrow

# for the test
from skytether.dataformats import BinaryFromTable, TableFromBinary, EncodeStr
from scytether import PyKineticConn


# ------------------------------
# reference variables

# >> constants
kv_mib = 1048576

# >> connections
py_kconn = PyKineticConn()
py_kconn.Connect()
if not py_kconn.IsConnected():
    sys.exit('Could not connect to kinetic')


# table_keyspace = 'expression:0165ebd1'
# table_keyspace = 'ebi-gxa/E-GEOD-106540'
# table_keyspace = 'ebi-gxa/E-CURD-6.clusters'
# table_keyspace = 'ebi-gxa.clusters'
# table_keyspace = 'test-geneexpr/T-DUPS-01'

table_keyspace = 'test/t-statistic'
tstat_table = py_kconn.ReadPartition(table_keyspace)
py_kconn.PrintDataStats()
sys.exit()

# if the metadata key is still in plaintext, test_meta is a string
# test_meta = py_kconn.ReadKeyValue(table_keyspace)

# if it is encoded in arrow IPC, then it is a schema
# test_meta = py_kconn.ReadPartitionMeta(table_keyspace)
# print(test_meta)

test_clustermap = pyarrow.Table.from_arrays(
     [
          pyarrow.array(numpy.array(['TestCluster-0', 'TestCluster-1']))
         ,pyarrow.array(numpy.array(['E-GEOD-100618', 'E-GEOD-100618']))
     ]
    ,schema=pyarrow.schema([
          pyarrow.field('cluster_name', pyarrow.string())
         ,pyarrow.field('partition'   , pyarrow.string())
     ])
)

print(test_clustermap.to_pandas())
py_kconn.UpdateIndex('ebi-gxa.clusters', test_clustermap, kv_mib)
sys.exit()

# print('Reading data before de-dup...')
# test_data = py_kconn.ReadPartition(table_keyspace)
# print(test_data.num_rows)
# print(len(test_data.schema))
# py_kconn.PrintDataStats()
# print(test_data.to_pandas())

# import pyarrow.compute
# cluster_counts = pyarrow.compute.value_counts(test_data.column(0))
# for count_dict in cluster_counts:
#     for tuple_key, tuple_val in count_dict.items():
#         print(f'{tuple_key} -> {tuple_val}')


# print('Dropping duplicates...')
# TODO: needs the keyspace split into domain and partition keys
# py_kconn.DropDuplicateRowsByKey(table_keyspace, 0)

# print('Reading data after de-dup...')
# test_data = py_kconn.ReadPartition(table_keyspace)
# print(test_data.to_pandas())

# cluster_names = list(map(EncodeStr, ['E-CURD-10-2', 'E-CURD-6-3']))
# print(f'getting data for clusters: "{cluster_names}"')
# partition_names = py_kconn.ReadPartitionsByCluster('ebi-gxa', cluster_names)
# print('partitions containing expression data:')
# print(partition_names.to_pandas())

# for data_chunk in partition_names.iterchunks():
#     for element in data_chunk:
#         partition_keyspace = element.as_py()
#
#         expr_data = py_kconn.ReadCellsByCluster(partition_keyspace, cluster_names)
#         print('gene expr for clusters:')
#         print(expr_data.to_pandas())

# gene_ids = test_data.column(0)
# print(gene_ids.to_pandas())
# print(gene_ids.num_chunks)

sys.exit()

# ------------------------------
# grab partition count from test_meta

# >> if the partition count is still in plain text
# partition_count = int(test_meta.split('partition_count:')[1].strip())

# >> if the partition count is encoded in arrow IPC
partition_count = int.from_bytes(
     test_meta.metadata['partition_count'.encode('utf-8')]
    ,byteorder='big'
)

print(f'partition count -> {partition_count}')

# ------------------------------
# read the first few partition slices

for slice_ndx in range(3):
    table_slicekey = f'{table_keyspace}.{slice_ndx}'

    test_df = py_kconn.ReadPartitionSlice(table_slicekey)
    table_slice  = test_df.slice(0, 10)
    print(table_slice.to_pandas())

sys.exit('Done')

# >> a little test for putting ipc in metadata
table_slice  = table_slice.replace_schema_metadata({'test_meta'.encode(): 'nested_table!'.encode()})
slice_binary = BinaryFromTable(table_slice)
new_schema = test_df.schema.with_metadata({
     'partition_count'.encode('utf-8'): partition_count.to_bytes(2, byteorder='big')
    ,'clusters'.encode('utf-8')       : slice_binary.to_pybytes()
})

print(f'\nNew Schema (with metadata): {new_schema.to_string(show_schema_metadata=False)}')
#print(f'\nNew Schema (metadata only): {new_schema.metadata}')
print(int.from_bytes(new_schema.metadata['partition_count'.encode('utf-8')], byteorder='big'))

# ------------------------------
print('\nResults of test:')
nested_table = TableFromBinary(new_schema.metadata['clusters'.encode('utf-8')])
print(nested_table.to_pandas())
print(nested_table.schema.to_string(show_schema_metadata=True))
# ------------------------------
# <<

# dataset_df = py_kconn.ReadPartition(table_keyspace).to_pandas()
# print(dataset_df.shape)
