# ------------------------------
# pylint directives

# pylint: disable=wrong-import-order
# pylint: disable=no-name-in-module
# pylint: disable=unused-import
# pylint: disable=missing-module-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=multiple-statements
# pylint: disable=unused-variable


# ------------------------------
# Dependencies

import os
import sys

import numpy
import scipy.io
import pandas

# for the test
from skytether.dataformats import BinaryFromTable, TableFromBinary, EncodeStr
from scytether import PyKineticConn, PyPartition


def ReadCSV(csv_filepath, skip=0, delim='\t'):
    """ Simplified CSV parser, that assumes tab-delimited columns. """
    if not os.path.isfile(csv_filepath): return None

    with open(csv_filepath, 'r', encoding='utf-8') as csv_handle:
        # list or row data, if the row isn't skipped
        row_data = [
            line.strip().split(delim)[0]
            for ndx, line in enumerate(csv_handle)
            if skip == 0 or ndx > skip
        ]

    return numpy.array(row_data)


def ViewKinetic(kconn, table_keyspace='ebi-gxa/E-GEOD-100618'):
    test_partition = kconn.ReadPartition(table_keyspace)
    test_data      = test_partition.data()
    # test_data = kconn.ReadCellsByCluster(table_keyspace, [EncodeStr('E-GEOD-98556-0')])
    # print('ebi-gxa/E-GEOD-98556;E-GEOD-98556-0')
    print(f'Total Row count: {test_data.num_rows}')
    print(f'Total Col count: {test_data.num_columns}')

    # print(test_data.to_pandas().iloc[:20, :20])
    print(test_data.to_pandas())
    kconn.PrintDataStats()


def ViewFilesystem(excerpt_start=5, excerpt_len=15):
    dataset_name = 'E-GEOD-100618'
    ebi_rootdir  = os.path.join('resources', 'sample-data', 'ebi')
    dataset_dir  = os.path.join(ebi_rootdir, dataset_name)

    mtx_path, row_path, col_path = '', '', ''
    for filename in os.listdir(dataset_dir):
        filepath = os.path.join(dataset_dir, filename)

        if   filename.endswith('.mtx'):      mtx_path = filepath
        elif filename.endswith('.mtx_rows'): row_path = filepath
        elif filename.endswith('.mtx_cols'): col_path = filepath

    mtx_rows = ReadCSV(row_path)
    mtx_cols = ReadCSV(col_path)
    mtx_data = pandas.DataFrame(
         scipy.io.mmread(mtx_path).todense()
        ,index=mtx_rows
        ,columns=mtx_cols
    )

    if mtx_rows is None or mtx_cols is None:
        sys.exit('Failed to parse MTX rows or cols')

    row_excerpt = mtx_rows[excerpt_start:excerpt_len]
    print('Row excerpt:\n\t{}'.format(
        '\n\t'.join(row_excerpt)
    ))

    col_excerpt = mtx_cols[excerpt_start:excerpt_len]
    print('Col excerpt:\n\t{}'.format(
        '\n\t'.join(col_excerpt)
    ))

    mtx_excerpt = mtx_data.loc[row_excerpt, col_excerpt]
    print(f'MTX excerpt:\n\t{mtx_excerpt}')


if __name__ == '__main__':
    eval_conn = PyKineticConn('eval-ebi')

    # eval_conn.Connect(host='10.23.1.172')
    eval_conn.Connect(host='10.23.90.2')
    if not eval_conn.IsConnected():
        sys.exit('Could not connect to kinetic')

    # kinetic_domain = 'ebi-test'
    # ViewKinetic(py_kconn, 'E-GEOD-98556')

    # ViewKinetic(eval_conn, 'ebi-evalcpu/E-GEOD-76312')
    ViewKinetic(eval_conn, 'ebi-evalcpu/E-GEOD-81608')

    # ViewFilesystem()

    del eval_conn

sys.stdout.write('<Complete>\n')
