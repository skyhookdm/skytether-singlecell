#!/usr/bin/env python

"""
Command-line script for loading an MTX-formatted gene expression matrix into kinetic.
"""


# ------------------------------
# pylint directives

# pylint: disable=invalid-name


# ------------------------------
# Dependencies

import os

from skytether.readers import ReadMTX
from skytether.skyhook import SkyhookPartitionMeta, SkyhookPartition


# ------------------------------
# Functions

def ReadDatasetAsMTX(mtx_rootdir, mtx_dirname):
    """
    Given a dataset name, looks up the MTX paths for the EBI dataset.
    """

    mtx_prefix = os.path.join(mtx_rootdir
        ,mtx_dirname
        ,f'{mtx_dirname}.aggregated_filtered_normalised_counts'
    )

    return ReadMTX(
         mtx_filepath=f'{mtx_prefix}_matrix.mtx'
        ,row_filepath=f'{mtx_prefix}.mtx_rows'
        ,col_filepath=f'{mtx_prefix}.mtx_cols'
    )

def AsMatrixPartition(partition_name, mtx_dataset):
    """
    Converts the MTX dataset paths, :mtx_dataset:, into a skyhook dataset partition.
    """

    partition_meta = SkyhookPartitionMeta(mtx_dataset.GeneExprSchemaForMatrix())
    partition_meta.SetName(partition_name)

    return (
        SkyhookPartition.WithMetadata(partition_meta)
                        .ForTable(mtx_dataset.TableForMatrix())
    )


# ------------------------------
# Main logic

if __name__ == '__main__':
    ebi_rootdir  = os.path.join('resources', 'sample-data', 'ebi')
    dataset_name = 'E-GEOD-124263'

    print('Constructing file paths...')
    dataset_mtxpaths = ReadDatasetAsMTX(ebi_rootdir, dataset_name)

    print('Constructing partition table...')
    skyhook_dataset  = AsMatrixPartition(dataset_name, dataset_mtxpaths)
    skyhook_dataset.WriteAll()
