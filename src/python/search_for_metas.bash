#!/usr/bin/bash

function keyrange() {
    key_count="${1:-500}"

    # echo "key count: ${key_count}\t start key: ${start_key}"
    if [[ -n "${2}" ]]; then
        /opt/libkinetic/bin/kctl -h 10.23.89.5 range -n ${key_count} -s "${2}"
    else
        /opt/libkinetic/bin/kctl -h 10.23.89.5 range -n ${key_count}
    fi
}


# for debugging
max_iters=$(( 2 ))
iter_ndx=$(( 1 ))

# iteration params
keyrange_len=$(( 500 ))

# first iteration
range_results=($(keyrange ${keyrange_len}))
result_count=${#range_results[@]}

while [[ ${result_count} -gt 0 ]]; do
    # prep for the next window
    last_ndx=$(( result_count - 1 ))
    excl_startkey="${range_results[last_ndx]}"

    for key_name in ${range_results[@]}; do
        if [[ $(echo ${key_name} | grep -vP "[.]\d") ]]; then
            # ${metadata_arr[metadata_ndx]}="${key_name}"
            echo "${key_name}" >> metadata-keys.txt
        fi
    done

    # grab the next window
    range_results=($(keyrange ${keyrange_len} "${excl_startkey}"))

    # for debugging
    iter_ndx=$(( $iter_ndx + 1 ))
    echo -ne "\rwindow ndx: ${iter_ndx}"
    # [[ ${iter_ndx} -gt ${max_iters} ]] && break
done
