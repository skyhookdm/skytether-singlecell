# for the test
from skytether.dataformats import BinaryFromTable, TableFromBinary, BinaryFromSchema

from scytether import ( ReadKeyValue
                       ,ReadPartitionSlice
                       ,ReadPartition
                       ,ReadPartitionMeta
                       ,WriteMetadata
                      )

metas_to_update = [
#     'expression:001518dd'
     'expression:00246399'
    ,'expression:00264f16'
    ,'expression:00404c96'
    ,'expression:005588e5'
    ,'expression:0058f4cf'
    ,'expression:009552b2'
    ,'expression:00b2e962'
    ,'expression:0105cb49'
    ,'expression:010f4a2a'
    ,'expression:01189c34'
    ,'expression:011e17e9'
    ,'expression:012b6f13'
    ,'expression:01403755'
    ,'expression:0165ebd1'
]

for dataset in metas_to_update:
    p_meta_as_string = ReadKeyValue(dataset)
    print('metadata as string:')
    print(p_meta_as_string)

    partition_count = int(p_meta_as_string.split('partition_count:')[1].strip())
    print(f'partition count -> {partition_count}')

    p_meta_from_batch = ReadPartitionSlice(f'{dataset}.0').schema

    metakey_pcount = 'partition_count'.encode('utf-8')
    metaval_pcount = partition_count.to_bytes(2, byteorder='big')
    new_schema     = p_meta_from_batch.with_metadata({ metakey_pcount: metaval_pcount })

    WriteMetadata(dataset, new_schema)

    '''
    # ------------------------------
    # a little test for putting ipc in metadata
    table_slice  = test_df.slice(0, 5)
    table_slice  = table_slice.replace_schema_metadata({'test_meta'.encode(): 'nested_table!'.encode()})
    slice_binary = BinaryFromTable(table_slice)
    # ------------------------------

    new_schema = test_df.schema.with_metadata({
         'partition_count'.encode('utf-8'): partition_count.to_bytes(2, byteorder='big')
        ,'clusters'.encode('utf-8')       : slice_binary.to_pybytes()
    })

    print(f'\nNew Schema (with metadata): {new_schema.to_string(show_schema_metadata=False)}')
    #print(f'\nNew Schema (metadata only): {new_schema.metadata}')
    print(int.from_bytes(new_schema.metadata['partition_count'.encode('utf-8')], byteorder='big'))

    # ------------------------------
    print('\nResults of test:')
    nested_table = TableFromBinary(new_schema.metadata['clusters'.encode('utf-8')])
    print(nested_table.to_pandas())
    print(nested_table.schema.to_string(show_schema_metadata=True))
    # ------------------------------

    # dataset_df = ReadPartition('expression:001518dd').to_pandas()
    # print(dataset_df.shape)
    '''
