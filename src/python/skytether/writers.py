"""
Module for code that writes data to files.
"""

# pylint: disable=unused-argument


# ------------------------------
# pylint directives

# pylint: disable=pointless-string-statement


# import logging
# import pyarrow

from skytether.dataformats import BinaryFromTable


def WriteIPCFromTable(arrow_table, media_handle):
    """ Write arrow table to file-like object, :media_handle:, in IPC format. """

    media_handle.write(BinaryFromTable(arrow_table))
    return True


def WriteIPCFileFromTable(arrow_table, table_filepath):
    """ Write arrow table to :table_filepath: in IPC format. """

    with open(table_filepath, 'wb') as output_filehandle:
        WriteIPCFromTable(arrow_table, output_filehandle)

    return table_filepath

def WriteKineticFromDataset(skytether_dataset, kinetic_keyspace):
    """ Write entire dataset to :kinetic_keyspace: in IPC format. """

    # >> writes data recordbatches and finalizes with metadata
    # for dataset_partition in skytether_dataset.updated_partitions():
    #     conn.PutTableBatches(kinetic_keyspace, dataset_partition)

    return None


'''
class SkyhookFileWriter(object):
    logger = logging.getLogger('{}.{}'.format(__module__, __name__))
    logger.setLevel(logging.INFO)

    @classmethod
    def write_partitions_to_arrow(cls, dataset, output_dir, batch_size=0):
        cls.logger.info('>>> writing data partitions into directory of arrow files')

        # tbl_part is table partition
        for ndx, tbl_part in data_wrapper.batched_table_partitions(batch_size=batch_size):
            # Generate path to binary file based on table partition metadata
            path_to_blob = os.path.join(
                output_dir,
                '{:05d}-{}.{}'.format(
                    ndx,
                    tbl_part.column_names[0],
                    arrow
                )
            )

            # Serialize arrow table to binary and write to file
            arrow_binary_data = BinaryFromTable(tbl_part)
            with open(path_to_blob, 'wb') as blob_handle:
                blob_handle.write(arrow_binary_data)

        cls.logger.info('<<< data written')
'''
