"""
A thin module that just imports functions and classes into the current namespace for use by R
scripts that use reticulate.
"""

# ------------------------------
# pylint directives

# pylint: disable=unused-import
# pylint: disable=wrong-import-order
# pylint: disable=invalid-name
# pylint: disable=no-name-in-module


# ------------------------------
# Dependencies

# >> Standard library classes
from dataclasses               import dataclass

# >> Standard library functions
from operator                  import attrgetter

# >> types and functions for R interface via reticulate

#   |> types
from pandas                    import DataFrame
from numpy                     import ndarray

#   |> functions
from numpy                     import array

# >> cython functions
from scytether                 import PyKineticConn
from scytether                 import ( UnionIndex
                                       ,UpdateIndex
                                       ,ExtendIndex
                                       ,DropDuplicates as DeduplicateGenes
                                       ,PrintDataStats
                                       ,PyExtendTable
                                      )

# >> functions
from skytether.writers         import WriteIPCFileFromTable
from skytether.dataformats     import EncodeStr

# wrapper functions, to be re-implemented later
from skytether.kctl            import KineticRange

# >> classes
from skytether.skyhook         import SkyhookPartition
from skytether.knowledge.genes import GeneInfo, HGNC

# >> variables
#    |> system-based constants
from skytether.skyhook         import KINETIC_KV_MAXSIZE

#    |> data-based constants
from skytether.skyhook         import SLICE_SUFFIX, INDEX_SUFFIX


# ------------------------------
# Module variables
CLUSTER_KEY  = 'clusters'
CENTROID_KEY = 'centroids'

META_LABELS = [
     'lab'
    ,'platform'
    ,'species'
    ,'stage'
    ,'batch'
    ,'tissue'
]


# ------------------------------
# Classes
@dataclass
class SingleCellMetadata:
    """
    A data class that contains metadata labels for managing single-cell experimental data.
    """

    lab     : [str, None] = None
    platform: str         = 'Platform'
    species : str         = 'hsapiens'
    stage   : str         = 'DevelopmentalStage'
    batch   : int         = 1
    tissue  : str         = 'Tissue'
    cluster : [str, None] = None

    def ToString(self, sep='_'):
        """ Returns metadata formatted as a single string for file names or key names. """

        return sep.join([
             self.lab
            ,self.platform
            ,self.species
            ,self.stage
            ,self.batch
            ,self.tissue
            ,self.cluster
        ])

    def ToSchemaMeta(self):
        """
        Returns attributes as a dict to be merged into a schema's metadata.
        """

        return {
             EncodeStr('lab'     ):  self.lab
            ,EncodeStr('platform'):  self.platform
            ,EncodeStr('species' ):  self.species
            ,EncodeStr('stage'   ):  self.stage
            ,EncodeStr('batch'   ):  self.batch
            ,EncodeStr('tissue'  ):  self.tissue
            ,EncodeStr('cluster' ):  self.cluster
        }


# ------------------------------
# Functions

def PartitionsFromDomain(domain_keyspace):
    """ Calls KineticRange, returning only partition names. """

    domain_keys    = KineticRange(
         key_start=f'{domain_keyspace}/'
        ,key_end  =f'{domain_keyspace}0'
    )

    def FnFilterPartitions(key_name):
        return (
                SLICE_SUFFIX not in key_name
            and INDEX_SUFFIX not in key_name
        )

    return list(filter(FnFilterPartitions, domain_keys))


def ScanPartition(domain_key, partition_key):
    """ Reads all RecordBatches for the given partition. """

    expr_partition = (
        SkyhookPartition.InDomain(domain_key)
                        .WithName(partition_key)
    )

    return expr_partition.ReadAll()


def ScanClusterMap(domain_key, cluster_key=CLUSTER_KEY):
    """ Reads all RecordBatches for the cluster map in the given domain. """

    cluster_map = (
        SkyhookPartition.InDomain(domain_key)
                        .WithName(cluster_key)
    )

    return cluster_map.ReadAll()


def ReadCellMetaForClusters(domain_key, partition_key, cluster_names):
    """ Reads metadata from the partition to find cluster membership. """

    expr_partition = (
        SkyhookPartition.InDomain(domain_key)
                        .WithName(partition_key)
    )

    return expr_partition.ReadCellClustersMeta(list(map(EncodeStr, cluster_names)))


def ReadCellDataForClusters(domain_key, partition_key, cluster_names):
    """ Reads metadata from the partition to find cluster membership. """

    print(f'Reading cell data in clusters:\n\t{cluster_names}')

    expr_partition = (
        SkyhookPartition.InDomain(domain_key)
                        .WithName(partition_key)
    )

    return expr_partition.ReadCellClusters(list(map(EncodeStr, cluster_names)))


def UpdateClusterMap(domain_key, cluster_map, index_key=CLUSTER_KEY):
    """ Updates the cluster map for the given domain. """

    domain_kconn   = PyKineticConn(domain_key)
    index_keyspace = f'.{index_key}'

    print(f'\nUpdating cluster map [{domain_key}{index_keyspace}]')
    print(cluster_map.to_pandas())

    domain_kconn.UpdateIndex(index_keyspace, cluster_map, KINETIC_KV_MAXSIZE)


def UpdateClusterLabels(domain_key, partition_key, cluster_labels, index_key=CLUSTER_KEY):
    """ Updates cluster labels for the given domain/partition. """

    print(f'Updating cluster labels for partition:\n\t[{domain_key}] {partition_key}')
    print(cluster_labels.to_pandas())

    domain_kconn   = PyKineticConn(domain_key)
    index_keyspace = f'/{partition_key}.{index_key}'
    domain_kconn.UnionIndex(index_keyspace, cluster_labels, KINETIC_KV_MAXSIZE)


def UpdateCentroids(domain_key, centroid_table, index_key=CENTROID_KEY):
    """ Updates cluster labels for the given domain/partition. """

    domain_kconn   = PyKineticConn(domain_key)
    index_keyspace = f'.{index_key}'
    print(f'Updating cluster centroids [{domain_key}{index_keyspace}]')
    print(centroid_table.to_pandas())

    domain_kconn.ExtendIndex(index_keyspace, centroid_table, KINETIC_KV_MAXSIZE)


def GetEnsemblGeneAnnotations():
    """ Returns a list of gene annotations that have known ensembl annotations. """

    gene_dict = HGNC.Load()

    # numpy ndarray of gene annotations to return to R
    GetEnsemblInfo    = attrgetter('ensembl', 'entrez', 'symbol')
    filtered_geneinfo = array([
        GetEnsemblInfo(gene_info)
        for symbol, gene_info in gene_dict.info.items()
        if (gene_info.ensembl and gene_info.entrez and gene_info.symbol)
    ])

    # match names to biomaRt package
    biomaRt_names = ['ensembl_gene_id', 'entrezgene_id', 'external_gene_name']

    return DataFrame(
         data=filtered_geneinfo
        ,columns=biomaRt_names
    )


def ArrowCBind(left_table, right_table):
    """ An interface to PyExtendTable for R. """

    return PyExtendTable(left_table, right_table)
