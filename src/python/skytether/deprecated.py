# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=wrong-import-order
# pylint: disable=pointless-string-statement
# pylint: disable=invalid-name
# pylint: disable=no-else-return
# pylint: disable=multiple-statements

import sys

# third-party
import scipy
import numpy
import pandas
import pyarrow

# >> classes
from dataclasses import dataclass

# >> functions
from skytether.util import BatchIndices

'''
# NOTE: From singlecell.GeneExpression

# TODO would have to also batch by cells, so maybe just do this later
def partition_by_genes(self, batch_size=0, gene_count=None, cell_count=10000):
    """
    Generator that iterates over partitions of :batch_size: columns, from 0 to :gene_count:. If
    used as is, this produces column indices from 0 to :gene_count: in N batches, where each
    batch has :batch_size: values and there are floor(:gene_count: / :batch_size:) number of
    batches.

    To shift the whole range, simply add a fixed value to the beginning of each batch; To have
    overlapping batches, add a value smaller than :batch_size: to each batch proceeding the
    first, etc.
    """

    # Partition by columns since we're using csc_matrix
    self.cells_as_records()

    # default gene_count to all genes; default batch_size to all genes
    if gene_count is None: gene_count = self.expression.shape[1]
    if batch_size == 0:    batch_size = self.expression.shape[1]

    for batch_id, start_ndx, end_ndx in batched_indices(gene_count, batch_size):
        yield (
             batch_id
            ,pyarrow.Table.from_arrays(
                  # TODO set row_start and row_end based on cell batching
                  self.arrays_for_partition(
                      start_ndx, end_ndx, row_end=cell_count
                  )
                 ,schema=self.gene_schema_for_partition(start_ndx, end_ndx)
             )
        )
'''

# ------------------------------
# Module Variables

# 1 MiB in bytes
KINETIC_KV_MAXSIZE = 1024 * 1024

# ------------------------------
# Classes

@dataclass
class Annotation:
    headers    : numpy.ndarray
    annotations: numpy.ndarray

    def __getitem__(self, col_name):
        return self.annotations[:, numpy.where(self.headers == col_name)]

    @property
    def shape(self):
        """Shape of expression matrix."""
        return self.annotations.shape

    def astype(self, dtype):
        self.annotations = self.annotations.astype(dtype=dtype, copy=False)

        return self

    def columns(self, from_col_ndx=None, to_col_ndx=None):
        return self.headers[from_col_ndx:to_col_ndx]

    def data(self, from_col=None, to_col=None, from_row=None, to_row=None):
        return self.annotations[from_row:to_row, from_col:to_col]

    def data_as_array(self, from_col=None, to_col=None, from_row=None, to_row=None):
        is_sparse_matrix = isinstance(self.annotations, (
             scipy.sparse.coo_matrix
            ,scipy.sparse.csc_matrix
        ))

        if is_sparse_matrix:
            data_array = self.annotations[from_row:to_row, from_col:to_col].toarray()

            if data_array.shape[1] == 1:
                return data_array[:,0]

            return data_array

        elif isinstance(self.annotations, numpy.matrix):
            return self.annotations[from_row:to_row, from_col:to_col].getA()

        elif isinstance(self.annotations, numpy.ndarray):
            return self.annotations[from_row:to_row, from_col:to_col]

        sys.exit(f'Not sure how to convert {type(self.annotations)} to {type(numpy.ndarray)}')


#@dataclass
class GeneExpression:
    # Maybe not a good fit for a dataclass
    # data : pyarrow.Table
    # genes: pyarrow.Schema
    # cells: pyarrow.Schema

    def __init__(self, expr, genes, cells, **kwargs):
        super().__init__(**kwargs)

        self.expression = expr
        self.genes      = genes
        self.cells      = cells

        self.expr_type = numpy.uint16
        self.row_type  = 'genes'

    @property
    def shape(self):
        """Shape of expression matrix."""
        return self.expression.shape

    def astype(self, dtype):
        self.expression = self.expression.astype(dtype=dtype, copy=False)

        return self

    def transpose(self):
        """ Transpose rows and columns. """

        # Make sure we change our cell/gene orientation
        if self.row_type == 'genes':
            self.row_type = 'cells'

        else:
            self.row_type = 'genes'

        # And then delegate the actual matrix transpose
        self.expression.transpose()

    def cell_schema_for_partition(self, from_cell_ndx=None, to_cell_ndx=None):
        return pyarrow.schema([
            (cell_id, self.expr_type)
            for cell_id in self.cells[from_cell_ndx:to_cell_ndx]
        ])

    def gene_schema_for_partition(self, from_gene_ndx=None, to_gene_ndx=None):
        return pyarrow.schema([
            (gene_name, self.expr_type)
            for gene_name in self.genes[from_gene_ndx:to_gene_ndx]
        ])

    def columns_for_partition(self, col_start=0, col_end=None):
        return self.expression[:, col_start:col_end]

    def rows_for_partition(self, row_start=0, row_end=None):
        return self.expression[row_start:row_end, :]

    def cells_as_records(self):
        if self.row_type == 'genes': self.transpose()

    def genes_as_records(self):
        if self.row_type != 'genes': self.transpose()

    def arrays_for_partition(self, col_start, col_end, row_start=None, row_end=None):
        expr_partition = self.expression[row_start:row_end, col_start:col_end]

        return list(map(
            numpy.ravel,
            numpy.hsplit(expr_partition, expr_partition.shape[1])
        ))

    def partition_by_cells(self, batch_size=0, cell_count=None):
        """
        Generator that iterates over partitions of :batch_size: columns, from 0 to :cell_count:. If
        used as is, this produces column indices from 0 to :cell_count: in N batches, where each
        batch has :batch_size: values and there are floor(:cell_count: / :batch_size:) number of
        batches.

        To shift the whole range, simply add a fixed value to the beginning of each batch; To have
        overlapping batches, add a value smaller than :batch_size: to each batch proceeding the
        first, etc.
        """

        # Partition by columns since we're using csc_matrix
        self.genes_as_records()

        # default cell_count to all cells; default batch_size to all cells
        if cell_count is None: cell_count = self.expression.shape[1]
        if batch_size == 0:    batch_size = self.expression.shape[1]

        for batch_id, start_ndx, end_ndx in BatchIndices(cell_count, batch_size):
            yield (
                 batch_id
                ,pyarrow.Table.from_arrays(
                      self.arrays_for_partition(start_ndx, end_ndx)
                     ,schema=self.cell_schema_for_partition(start_ndx, end_ndx)
                 )
            )

    def SubsampleByCounts(self, gene_count=100, cell_count=10):
        if isinstance(self.expression, scipy.sparse.coo_matrix):
            self.expression = self.expression.tocsc().todense()

        self.expression = self.expression[:gene_count, :cell_count]
        if self.row_type != 'genes':
            self.expression = self.expression[:cell_count, :gene_count]

        self.genes      = self.genes[:gene_count]
        self.cells      = self.cells[:cell_count]

        return self


""" This is deprecated in favor of a more robust sizing approach. """
@dataclass
class MTXDataset:
    """ Dataclass holding references to data for an MTX dataset.  """

    mtx    : scipy.sparse.coo_matrix
    row    : numpy.ndarray
    col    : numpy.ndarray
    schema : pyarrow.Schema = None


    def _SetSchema(self, dataset_schema):
        """ A setter for the schema attribute. Mostly a placeholder for now. """
        self.schema = dataset_schema

    def GeneExprSchemaForMatrix(self):
        """ Constructs schema assuming the data is for single-cell gene expression. """

        # NOTE: we hard-code gene IDs as a first column for temporary convenience.
        self._SetSchema(pyarrow.schema(
               [ pyarrow.field('gene_id', pyarrow.string()) ]
             + [
                   pyarrow.field(cell_id, pyarrow.uint16())
                   for cell_id in self.col
               ]
            ,metadata={
                 geneid.encode(): (ndx.to_bytes(2, byteorder='big'))
                 for ndx, geneid in enumerate(self.row)
             }
        ))

        return self.schema

    def GeneExprSchemaForRelation(self):
        """ Constructs schema assuming the data is for single-cell gene expression. """
        self._SetSchema(pyarrow.schema(
             [
                  pyarrow.field('gene_id' , pyarrow.string())
                 ,pyarrow.field('cell_id' , pyarrow.string())
                 ,pyarrow.field('expr_val', pyarrow.uint16())
             ]
        ))

        return self.schema

    def SlicesForMatrix(self, matrix_dtype=numpy.uint16, batch_size=None):
        """
        Returns data from the MTX dataset formatted as a matrix-style Arrow Table (de-normalized
        into N columns, where each column is a single-cell).
        """

        matrix_schema = self.GeneExprSchemaForMatrix()

        # use pandas to convert the coordinate-based matrix
        matrix_df = pandas.DataFrame.sparse.from_spmatrix(
             scipy.sparse.csc_matrix(self.mtx, dtype=matrix_dtype)
            ,index=self.row
            ,columns=self.col
        )

        # Try to compute a batch size based on kinetic kv max size
        if not batch_size:
            # conservative, per-element size, assuming lz4 compression
            element_size = 1.5 * matrix_dtype().itemsize
            batch_size   = int((KINETIC_KV_MAXSIZE / (element_size * matrix_df.shape[1])))

        # Construct RecordBatches
        record_batches = []
        for batch_row_start in range(0, matrix_df.shape[0], batch_size):
            batch_row_end = min(batch_row_start + batch_size, matrix_df.shape[0])

            batch_index_start = self.row[batch_row_start]
            batch_index_end   = self.row[batch_row_end - 1]

            # NOTE: first column is row IDs, for temporary convenience
            batch_columndata = [self.row[batch_row_start:batch_row_end]]

            # slice the data
            batch_columndata += [
                numpy.array(
                     matrix_df.loc[batch_index_start:batch_index_end, col_id]
                    ,dtype=matrix_dtype
                )
                for col_id in matrix_df.columns
            ]

            # add this record batch, using the matrix-style schema
            record_batches.append(
                pyarrow.RecordBatch.from_arrays(
                     batch_columndata
                    ,schema=matrix_schema
                )
            )

        # Return a list of record batches
        return record_batches

    def SlicesForRelation(self, batch_size=None):
        """
        Returns data from the MTX dataset formatted as a relational-style Arrow Table (normalized
        into 3 columns).
        """

        relational_schema = self.GeneExprSchemaForRelation()
        expr_dtype        = numpy.uint16

        # Try to compute a batch size based on kinetic kv max size
        if not batch_size:
            batch_size   = int(
                KINETIC_KV_MAXSIZE
                / ( 1.5 * (  self.row.dtype.itemsize
                           + self.col.dtype.itemsize
                           + numpy.uint16().itemsize
                          )
                  )
            )

        record_batches = []
        for batch_row_start in range(0, self.mtx.shape[0], batch_size):
            batch_row_end = batch_row_start + batch_size

            row_coords = self.mtx.row[batch_row_start:batch_row_end]
            col_coords = self.mtx.col[batch_row_start:batch_row_end]
            mtx_data   = self.mtx.data[batch_row_start:batch_row_end]

            row_ids   = self.row[row_coords]
            col_ids   = self.col[col_coords]
            expr_vals = numpy.array(list(map(int, mtx_data)), dtype=expr_dtype)

            record_batches.append(
                pyarrow.RecordBatch.from_arrays(
                     [ row_ids, col_ids, expr_vals ]
                    ,schema=relational_schema
                )
            )

        # Return a list of record batches
        return record_batches

        # return pyarrow.Table.from_batches(record_batches)
