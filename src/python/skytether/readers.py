#!/usr/bin/env python
"""
This module provides higher-level interfaces for reading data from various data sources.
"""

# ------------------------------
# pylint directives

# pylint: disable=wrong-import-order
# pylint: disable=multiple-statements
# pylint: disable=no-value-for-parameter

# ------------------------------
# Dependencies

import os
import datetime

import scipy.io
import scipy.sparse

import numpy
import pandas
import pyarrow

# >> classes
from dataclasses import dataclass

# >> functions
from scytether import FindOptimalRowCount

from skytether.dataformats import ( TableFromBinary
                                   ,BinaryFromTable
                                   ,NumpySliceFromDataFrame
                                   ,SlicesFromDataFrame
                                  )

# >> variables
from skytether.skyhook import KINETIC_KV_MAXSIZE


# ------------------------------
# Module-level Variables

DEFAULT_NUMPY_TYPE = numpy.float64
DEFAULT_ARROW_TYPE = pyarrow.float64()

SAMPLE_ROWCOUNT = 256
STRIPE_BYTESIZE = 64 * KINETIC_KV_MAXSIZE


# ------------------------------
# Functions

def ReadCSV(csv_filepath, skip=0, delim='\t'):
    """ Simplified CSV parser, that assumes tab-delimited columns. """
    if not os.path.isfile(csv_filepath): return None

    with open(csv_filepath, 'r') as csv_handle:
        # list or row data, if the row isn't skipped
        row_data = [
            line.strip().split(delim)[0]
            for ndx, line in enumerate(csv_handle)
            if skip == 0 or ndx > skip
        ]

    return numpy.array(row_data)


def ReadMTX(mtx_filepath, row_filepath, col_filepath):
    """ A constructor for MTXDataset that takes reads files """
    return MTXDataset(
         scipy.io.mmread(mtx_filepath)
        ,ReadCSV(row_filepath)
        ,ReadCSV(col_filepath)
    )


def ReadIPCFromFile(ipc_filepath):
    """ A function to read IPC-formatted data from :ipc_filepath:. """

    with open(ipc_filepath, 'rb') as file_inhandle:
        data_table = TableFromBinary(file_inhandle.read())

    return data_table


# ------------------------------
# Classes

@dataclass
class MTXDataset:
    """ Dataclass holding references to data for an MTX dataset.  """

    mtx    : scipy.sparse.coo_matrix
    row    : numpy.ndarray
    col    : numpy.ndarray
    schema : pyarrow.Schema = None

    def _SetSchema(self, dataset_schema):
        """ A setter for the schema attribute. Mostly a placeholder for now. """
        self.schema = dataset_schema

    def GeneExprSchemaForMatrix(self):
        """ Constructs schema assuming the data is for single-cell gene expression. """

        meta_genendx = {}

        # not needed while we add a column for the gene IDs
        # meta_genendx = {
        #     geneid.encode(): (ndx.to_bytes(2, byteorder='big'))
        #     for ndx, geneid in enumerate(self.row)
        # }

        # NOTE: we hard-code gene IDs as a first column for temporary convenience.
        self._SetSchema(pyarrow.schema(
               [ pyarrow.field('gene_id', pyarrow.string()) ]
             + [
                   pyarrow.field(cell_id, DEFAULT_ARROW_TYPE)
                   for cell_id in self.col
               ]
            ,metadata=meta_genendx
        ))

        return self.schema

    def GeneExprSchemaForRelation(self):
        """ Constructs schema assuming the data is for single-cell gene expression. """
        self._SetSchema(pyarrow.schema(
             [
                  pyarrow.field('gene_id' , pyarrow.string())
                 ,pyarrow.field('cell_id' , pyarrow.string())
                 ,pyarrow.field('expr_val', DEFAULT_ARROW_TYPE)
             ]
        ))

        return self.schema

    def TableForMatrix(self, matrix_dtype=DEFAULT_NUMPY_TYPE):
        """
        Returns data from the MTX dataset formatted as a matrix-style Arrow Table (de-normalized
        into N columns, where each column is a single-cell).
        """

        # use pandas to convert the coordinate-based matrix
        print(f'[{datetime.datetime.now()}] Constructing schema and sparse dataframe...')
        matrix_schema = self.GeneExprSchemaForMatrix()
        matrix_df     = pandas.DataFrame.sparse.from_spmatrix(
             scipy.sparse.csc_matrix(self.mtx, dtype=matrix_dtype)
            ,index=self.row
            ,columns=self.col
        )

        # find optimal row count
        print(f'[{datetime.datetime.now()}] Constructing sample slice...')
        sample_slice = pyarrow.Table.from_arrays(
             NumpySliceFromDataFrame(matrix_df, self.row, SAMPLE_ROWCOUNT, matrix_dtype)
            ,schema=matrix_schema
        )

        print(f'[{datetime.datetime.now()}] Determining optimal row count...')
        row_batchsize = FindOptimalRowCount(sample_slice, KINETIC_KV_MAXSIZE, SAMPLE_ROWCOUNT)

        print(f'[{datetime.datetime.now()}] Recommended row count: {row_batchsize}')

        if not row_batchsize:
            print(
                f'[{datetime.datetime.now()}] Determining optimal stripe row count... '
            )

            row_batchsize = FindOptimalRowCount(
                sample_slice, STRIPE_BYTESIZE, SAMPLE_ROWCOUNT
            )

            print(f'[{datetime.datetime.now()}] Recommended stripe row count: {row_batchsize}')

        # NOTE: for debugging striping
        # sys.exit()

        print(f'[{datetime.datetime.now()}] Creating table from table slices...')
        return pyarrow.concat_tables((
            pyarrow.Table.from_arrays(table_slice, schema=matrix_schema)
            for table_slice in SlicesFromDataFrame(
                matrix_df, self.row, row_batchsize, matrix_dtype
            )
        ))

    def TableForRelation(self):
        """
        Returns data from the MTX dataset formatted as a relational-style Arrow Table (normalized
        into 3 columns).
        """

        expr_dtype        = DEFAULT_NUMPY_TYPE
        relational_schema = self.GeneExprSchemaForRelation()

        return pyarrow.Table.from_arrays(
             [
                  pyarrow.array(self.row[self.mtx.row])
                 ,pyarrow.array(self.col[self.mtx.col])
                 ,pyarrow.array(numpy.array(self.mtx.data, dtype=expr_dtype))
             ]
            ,schema=relational_schema
        )


if __name__ == '__main__':
    ebi_rootdir   = os.path.join('resources', 'sample-data', 'ebi')
    test_basename = os.path.join(ebi_rootdir
        ,'E-CURD-10'
        ,'E-CURD-10.aggregated_filtered_normalised_counts'
    )

    test_dataset_partition = ReadMTX(
         mtx_filepath=f'{test_basename}_matrix.mtx'
        ,row_filepath=f'{test_basename}.mtx_rows'
        ,col_filepath=f'{test_basename}.mtx_cols'
    )

    # read in gene expression matrix
    umi = test_dataset_partition.TableForMatrix()

    # >> Using feather (defaults to lz4 compression)
    # feather.write_feather(umi, 'umi.arrow')

    # >> Uses IPC format to write to serialize in memory then write as binary to a file
    with open('umi.arrow', 'wb') as output_filehandle:
        output_filehandle.write(BinaryFromTable(umi))

    print(umi.to_pandas())
    print(umi.shape)
