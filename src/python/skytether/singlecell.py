"""
TODO: this needs a lot of changing to be simplified and changed in the direction of a more
practical implementation.

Module that handles domain-specific data management for single-cell bioinformatics.
"""

# pylint: disable=wrong-import-order
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=invalid-name
# pylint: disable=multiple-statements
# pylint: disable=no-else-return

# ------------------------------
# Dependencies

# third-party
import pyarrow

# >> precise imports
#    >  classes
from dataclasses import dataclass

#    >  functions
from scytether import WritePartition


# ------------------------------
# Functions


# ------------------------------
# Classes


@dataclass
class GeneExpression:
    """
    A data domain representing gene expression matrices from various kinds of single-cell RNA
    sequencing machines.
    """

    data : pyarrow.Table
    genes: pyarrow.Table = None
    cells: pyarrow.Table = None

    def DirtyPartitions(self):
        """ Generator for Construct metadata for gene expression identified by :partition_key:. """

    def MetaForPartition(self, partition_key=None):
        """ Construct metadata for gene expression identified by :partition_key:. """

    def DataForPartition(self, partition_key=None):
        """ Construct data RecordBatches for gene expression identified by :partition_key:. """

    def WriteAll(self, partition_namekey='partition_name', batch_size=20480):
        """ Naive function to write expression to kinetic. """
        meta_namekey = partition_namekey.encode('utf-8')

        data_keyspace = (self.data.schema.metadata[meta_namekey].decode())
        WritePartition(data_keyspace, self.data, batch_size)

        if self.genes is not None:
            gene_keyprefix = self.genes.schema.metadata[meta_namekey].decode()
            WritePartition(f'{gene_keyprefix}.genes', self.data, batch_size)

        if self.cells is not None:
            cell_keyprefix = self.cells.schema.metadata[meta_namekey].decode()
            WritePartition(f'{cell_keyprefix}.cells', self.data, batch_size)


@dataclass
class CellCluster:
    """
    A data domain representing clusters of similar cells. This is for "single-cells" from various
    kinds of single-cell RNA sequencing machines.
    """

    clusters  : list[str]
    partitions: list[str]

    def WriteKineticMetadata(self):
        """ Write RecordBatches for centroids of this cluster. """

    def WriteKineticBatches(self):
        """ Write RecordBatches for centroids of this cluster. """
