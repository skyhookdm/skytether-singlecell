"""
Module for interfacing with gene annotation knowledge.

Author: Aldrin Montana
"""


# ------------------------------
# pylint directives

# pylint: disable=wrong-import-order
# pylint: disable=invalid-name
# pylint: disable=multiple-statements


# ------------------------------
# Dependencies

import os
import sys
import requests
import json

from operator    import itemgetter
from collections import OrderedDict
from dataclasses import dataclass


# ------------------------------
# Module Variables

# NOTE: sys.argv[0] is going to be whatever script was invoked, not necessarily this module
module_dirpath   = os.path.dirname(os.path.abspath(sys.argv[0]))
resource_dirpath = os.path.join(os.path.dirname(module_dirpath), 'resources')


# ------------------------------
# Classes

@dataclass
class GeneInfo:
    """ Data class for gene annotations. """

    symbol : str
    hgnc   : str
    refseq : str
    ensembl: str
    entrez : str

    @classmethod
    def FromHGNCDict(cls, hgnc_dict):
        """
        Builder function that returns a GeneInfo instance given a dictionary. It is assumed that
        the dictionary has the same key names as what the HGNC website specifies.
        """

        refseq_acc = hgnc_dict.get('refseq_accession')
        if refseq_acc: refseq_acc = refseq_acc[0]

        return cls(
             hgnc_dict['symbol']
            ,hgnc_dict['hgnc_id']
            ,refseq_acc
            ,hgnc_dict.get('ensembl_gene_id')
            ,hgnc_dict.get('entrez_id')
        )

    def __hash__(self):
        return hash(self.symbol)

    def __lt__(self, other_gene):
        return self.Id()  < other_gene.Id()

    def __gt__(self, other_gene):
        return self.Id()  > other_gene.Id()

    def __le__(self, other_gene):
        return self.Id() <= other_gene.Id()

    def __ge__(self, other_gene):
        return self.Id() >= other_gene.Id()

    def __eq__(self, other_gene):
        return self.Id() == other_gene.Id()

    def __ne__(self, other_gene):
        return self.Id() != other_gene.Id()

    def Id(self):
        """ Returns an ID for GeneInfo that is used for comparisons. """

        return int(self.hgnc[5:])


class HGNC:
    """
    A class for interacting with HGNC data.
    """

    hgnc_filename = 'hgnc_complete_set.json'
    hgnc_datakey  = 'response'
    hgnc_genekey  = 'docs'

    hgnc_dburi   = os.path.join('https://ftp.ebi.ac.uk', 'pub', 'databases', 'genenames', 'hgnc')
    hgnc_jsonuri = os.path.join(hgnc_dburi, 'json', hgnc_filename)
    hgnc_localuri = os.path.join(resource_dirpath, 'knowledge', 'hgnc')


    @classmethod
    def RequestCompleteSet(cls, data_uri=hgnc_jsonuri):
        """
        Requests the current version of HGNC data from :data_uri:.

        :data_uri: defaults to the HGNC server's endpoint for 'hgnc_complete_set.json'.

        Returns the dictionary retrieved from HGNC, or a dictionary containing error information
        usable for debugging.
        """

        hgnc_response = requests.get(data_uri)

        if hgnc_response.status_code != 200:
            return {
                 'status code': hgnc_response.status_code
                ,'message'    : 'failed to request complete set'
            }

        # HGNC returns data as JSON
        hgnc_jsondict = hgnc_response.json()

        # Return the actual data, or a dictionary for debugging
        return hgnc_jsondict.get(
             cls.hgnc_datakey
            ,{
                  'message'  : f'Could not find data key "{cls.hgnc_datakey}"'
                 ,'hgnc keys': hgnc_jsondict.keys()
             }
        )

    @classmethod
    def CreateLocalMirror(cls, mirror_dirpath=hgnc_localuri):
        """
        Creates, or returns the path to, a local mirror of files we request from the HGNC server.
        """

        # create a directory to put files
        if not os.path.isdir(mirror_dirpath):
            os.makedirs(mirror_dirpath)

        # define the files we expect to use
        geneinfo_filepath = os.path.join(mirror_dirpath, cls.hgnc_filename)

        if not os.path.isfile(geneinfo_filepath):
            # grab the file (use default URI)
            hgnc_geneinfo = cls.RequestCompleteSet()

            # Write the files to our directory of mirror files
            with open(geneinfo_filepath, 'w', encoding='utf-8') as geneinfo_filehandle:
                geneinfo_filehandle.write(json.dumps(hgnc_geneinfo))

        # return where we wrote it
        return geneinfo_filepath


    @classmethod
    def Load(cls, local_uri=hgnc_localuri):
        """ Loads HGNC data from a local mirror. """

        geneinfo_filepath = cls.CreateLocalMirror(local_uri)
        with open(geneinfo_filepath, 'r', encoding='utf-8') as geneinfo_filehandle:
            hgnc_data = json.loads(geneinfo_filehandle.read())

        gene_info = OrderedDict(
            sorted(
                 [
                     (gene_dict['symbol'], GeneInfo.FromHGNCDict(gene_dict))
                     for gene_dict in hgnc_data.get(cls.hgnc_genekey, [])
                 ]
                ,key=itemgetter(1)
            )
        )

        return cls(gene_info)

    def __init__(self, gene_info, **kwargs):
        super().__init__(**kwargs)

        self.info = gene_info

    def __getitem__(self, item_key):
        return self.info.__getitem__(item_key)

    def __contains__(self, item):
        return self.info.__contains__(item)


    def Get(self, item_key, default=None):
        """
        Delegates to the `get` method of info, an OrderedDict.
        """

        return self.info.get(item_key, default)


# ------------------------------
# Main Logic

if __name__ == '__main__':
    hgnc_knowledge = HGNC.Load()

    print(hgnc_knowledge.Get('A1BG'))
