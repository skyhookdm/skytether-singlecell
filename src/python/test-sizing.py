#!/usr/bin/env python

"""
Command-line script for loading an MTX-formatted gene expression matrix into kinetic.
"""


# ------------------------------
# pylint directives

# pylint: disable=invalid-name


# ------------------------------
# Dependencies

import pyarrow

from skytether.dataformats import TableFromBinary, BinaryFromTable, BinaryFromSchema
from scytether import FindOptimalRowCount


# ------------------------------
# Main logic

if __name__ == '__main__':
    table_parts = []
    for slice_ndx in range(3):
        with open(f'test.slice.{slice_ndx}', 'rb') as part_handle:
            slice_bin = part_handle.read()
            tbl_slice = TableFromBinary(slice_bin)
            schemabin_tbl = BinaryFromSchema(tbl_slice.schema)

            print(f'[slice] schema bin size: {len(schemabin_tbl)}')
            print(f'[slice] binary length  : {len(slice_bin)}')
            print(f'[slice] row count      : {tbl_slice.num_rows}\n')

            table_parts.append(tbl_slice)

            new_slice    = tbl_slice.replace_schema_metadata({})
            new_slicebin = BinaryFromTable(new_slice)
            schemabin_slice = BinaryFromSchema(new_slice.schema)

            print(f'[new-slice] schema bin size: {len(schemabin_slice)}')
            print(f'[new-slice] binary length  : {len(new_slicebin)}')
            print(f'[new-slice] row count      : {new_slice.num_rows}\n')


    tbl_part = pyarrow.concat_tables(table_parts)
    tbl_bin  = BinaryFromTable(tbl_part)
    tbl_schemabin = BinaryFromSchema(tbl_part.schema)

    print(f'[part] schema bin size: {len(tbl_schemabin)}')
    print(f'[part] binary length  : {len(tbl_bin)}')
    print(f'[part] row count      : {tbl_part.num_rows}\n')

    suggested_rowcount = FindOptimalRowCount(tbl_part, 1024 ** 2)
    print(f'[part] suggested row count: {suggested_rowcount}')

    suggested_rowcount = FindOptimalRowCount(tbl_part.combine_chunks(), 1024 ** 2)
    print(f'[part] suggested row count: {suggested_rowcount}')


    new_part = tbl_part.replace_schema_metadata({})
    new_bin  = BinaryFromTable(new_part)
    new_schemabin = BinaryFromSchema(new_part.schema)

    print(f'[new-part] schema bin size: {len(new_schemabin)}')
    print(f'[new-part] binary length: {len(new_bin)}')
    print(f'[new-part] row count    : {new_part.num_rows}\n')

    suggested_rowcount = FindOptimalRowCount(new_part, 1024 ** 2)
    print(f'[new-part] suggested row count: {suggested_rowcount}')


    suggested_rowcount = FindOptimalRowCount(new_part.combine_chunks(), 1024 ** 2)
    print(f'[new-part] suggested row count: {suggested_rowcount}')
