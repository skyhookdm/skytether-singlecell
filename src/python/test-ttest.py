# ------------------------------
# pylint directives

# pylint: disable=wrong-import-order
# pylint: disable=no-name-in-module
# pylint: disable=unused-import
# pylint: disable=missing-module-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=multiple-statements
# pylint: disable=unused-variable
# pylint: disable=invalid-name


# ------------------------------
# Dependencies

import os
import sys
import datetime

import numpy
import scipy.io
import pandas
import pyarrow

# for the test
from skytether.skyteth_r import GetEnsemblGeneAnnotations
from skytether.dataformats import BinaryFromTable, TableFromBinary, EncodeStr

from scytether import PyKineticConn

from scytether import ( ReadPartition
                       ,PyExtendTable
                       ,DiffExprCellClusters
                       ,ReadCellsByCluster
                       ,PrintDataStats
                       ,WritePartition
                      )



# ------------------------------
# Functions

def CompareCentroids():
    return kconn.DiffExprCentroids(
         domain_key='eval-ebi'
        ,left_colnames=[
              'gene_symbol'.encode('utf-8')
             ,'E-GEOD-100618-0'.encode('utf-8')
             ,'E-GEOD-100618-1'.encode('utf-8')
             ,'E-GEOD-100618-2'.encode('utf-8')
             ,'E-GEOD-100618-3'.encode('utf-8')
             ,'E-GEOD-100618-4'.encode('utf-8')
             ,'E-GEOD-76312-0'.encode('utf-8')
             ,'E-GEOD-76312-1'.encode('utf-8')
             ,'E-GEOD-76312-2'.encode('utf-8')
             ,'E-GEOD-76312-3'.encode('utf-8')
             ,'E-GEOD-76312-4'.encode('utf-8')
             ,'E-GEOD-76312-5'.encode('utf-8')
             ,'E-GEOD-76312-6'.encode('utf-8')
             ,'E-GEOD-76312-7'.encode('utf-8')
             # NOTE: fix clustering
             # ,'E-GEOD-76312-8'.encode('utf-8')
         ]
        ,right_colnames=[
              'gene_symbol'.encode('utf-8')
             ,'E-GEOD-106540-0'.encode('utf-8')
             ,'E-GEOD-106540-1'.encode('utf-8')
             ,'E-GEOD-106540-2'.encode('utf-8')
             ,'E-GEOD-106540-3'.encode('utf-8')
             ,'E-GEOD-106540-4'.encode('utf-8')
             ,'E-GEOD-106540-5'.encode('utf-8')
         ]
    )


def NormalizeGenes(gene_order, expr_table):
    # set the dataframe index
    sys.stdout.write(f'[{datetime.datetime.now()}] Converting arrow to pandas\n')
    sys.stdout.flush()
    expr_tabledf = expr_table.to_pandas()
    expr_tabledf.set_index('gene_id', inplace=True)

    # a new, filled dataframe to copy expr_tabledf values into
    sys.stdout.write(f'[{datetime.datetime.now()}] Creating expanded dataframe\n')
    sys.stdout.flush()
    filtered_expr_df = pandas.DataFrame(
         data=numpy.zeros(shape=(len(gene_order), expr_tabledf.shape[1]))
        ,index=gene_order
        ,columns=expr_tabledf.columns
        ,dtype=numpy.float64
    )

    sys.stdout.write(f'[{datetime.datetime.now()}] Filling expanded dataframe\n')
    sys.stdout.flush()
    known_geneexpr = expr_tabledf.loc[numpy.isin(expr_tabledf.index, filtered_expr_df.index)]
    filtered_expr_df.loc[known_geneexpr.index] = known_geneexpr
    # NOTE: keeping around for now
    # expect that gene_order is a list of genes in the order we want
    # is_gene_missing = numpy.isin(gene_order, expr_tabledf['gene_id'], invert=True)
    # missing_genes   = gene_order[is_gene_missing]
    # sys.stdout.write(
    #     f'[{datetime.datetime.now()}] {len(missing_genes)} missing genes\n'
    # )
    # sys.stdout.flush()

    # for missing_geneid in missing_genes:
    #     expr_tabledf.loc[missing_geneid] = pandas.Series(
    #          name=missing_geneid
    #         ,dtype=numpy.float64
    #     )

    # sys.stdout.write(f'[{datetime.datetime.now()}] filling missing values\n')
    # expr_tabledf.fillna(0.0, inplace=True)

    # Return a table with the genes in the correct order
    sys.stdout.write(
        f'[{datetime.datetime.now()}] Expanded dataframe complete '
        f'({filtered_expr_df.shape}) \n'
    )
    print(filtered_expr_df)
    sys.stdout.flush()

    return pyarrow.Table.from_pandas(filtered_expr_df.reset_index(drop=True))


def CompareCells():
    #gene_symbols_ordered = ReadPartition('eval-ebi.centroids').column('gene_symbol').to_numpy()
    ensembl_syms = GetEnsemblGeneAnnotations()['ensembl_gene_id']
    sys.stdout.write(f'[{datetime.datetime.now()}] {len(ensembl_syms)} genes\n')
    sys.stdout.flush()

    # >> Read data to be compared
    # NOTE: read cells from metacluster 12 (hard-coded for now)
    # NOTE: should normalize 'gene_id' vs 'gene_symbol'

    #   |> clusters from first dataset for metacluster 12
    left_metacluster_1 = ReadCellsByCluster(
         'eval-ebi/E-GEOD-100618'
        ,[
             'E-GEOD-100618-0'.encode('utf-8')
            ,'E-GEOD-100618-1'.encode('utf-8')
            ,'E-GEOD-100618-2'.encode('utf-8')
            ,'E-GEOD-100618-3'.encode('utf-8')
            ,'E-GEOD-100618-4'.encode('utf-8')
         ]
    )

    sys.stdout.write(f'[{datetime.datetime.now()}] Left metacluster 1:\n')
    sys.stdout.write(f'\t\trows: {left_metacluster_1.num_rows}\n')
    sys.stdout.write(f'\t\tcols: {left_metacluster_1.num_columns}\n')
    sys.stdout.flush()

    left_metacluster_1 = NormalizeGenes(ensembl_syms, left_metacluster_1)
    sys.stdout.write(f'[{datetime.datetime.now()}] Normalized rows:\n')
    sys.stdout.write(f'\t\tnormalized rows: {left_metacluster_1.num_rows}\n')
    sys.stdout.flush()

    #   |> clusters from second dataset for metacluster 12
    left_metacluster_2 = ReadCellsByCluster(
         'eval-ebi/E-GEOD-76312'
        ,[
             'E-GEOD-76312-0'.encode('utf-8')
            ,'E-GEOD-76312-1'.encode('utf-8')
            ,'E-GEOD-76312-2'.encode('utf-8')
            ,'E-GEOD-76312-3'.encode('utf-8')
            ,'E-GEOD-76312-4'.encode('utf-8')
            ,'E-GEOD-76312-5'.encode('utf-8')
            ,'E-GEOD-76312-6'.encode('utf-8')
            ,'E-GEOD-76312-7'.encode('utf-8')
            # ,'E-GEOD-76312-8' TODO
         ]
    )

    sys.stdout.write(f'[{datetime.datetime.now()}] Left metacluster 2:\n')
    sys.stdout.write(f'\t\trows: {left_metacluster_2.num_rows}\n')
    sys.stdout.write(f'\t\tcols: {left_metacluster_2.num_columns}\n')
    sys.stdout.flush()

    left_metacluster_2 = NormalizeGenes(ensembl_syms, left_metacluster_2)
    sys.stdout.write(f'[{datetime.datetime.now()}] Normalized rows:\n')
    sys.stdout.write(f'\t\tnormalized rows: {left_metacluster_2.num_rows}\n')
    sys.stdout.flush()

    #   |> combine clusters from both datasets to form metacluster 12
    left_metacluster_joined = PyExtendTable(left_metacluster_1, left_metacluster_2)
    sys.stdout.write(f'[{datetime.datetime.now()}] Joined left child clusters:\n')
    sys.stdout.write(f'\t\tmetacluster cols: {left_metacluster_joined.num_columns}\n')
    sys.stdout.flush()

    # NOTE: read cells from metacluster 13 (hard-coded for now)
    right_metacluster = ReadCellsByCluster(
         'eval-ebi/E-GEOD-106540'
        ,[
             'E-GEOD-106540-0'.encode('utf-8')
            ,'E-GEOD-106540-1'.encode('utf-8')
            ,'E-GEOD-106540-2'.encode('utf-8')
            ,'E-GEOD-106540-3'.encode('utf-8')
            ,'E-GEOD-106540-4'.encode('utf-8')
         ]
    )

    sys.stdout.write(f'[{datetime.datetime.now()}] Right metacluster:\n')
    sys.stdout.write(f'\t\trows: {right_metacluster.num_rows}\n')
    sys.stdout.write(f'\t\tcols: {right_metacluster.num_columns}\n')
    sys.stdout.flush()

    right_metacluster = NormalizeGenes(ensembl_syms, right_metacluster)
    sys.stdout.write(f'[{datetime.datetime.now()}] Normalized rows:\n')
    sys.stdout.write(f'\t\tnormalized rows: {right_metacluster.num_rows}\n')
    sys.stdout.flush()

    return DiffExprCellClusters(left_metacluster_joined, right_metacluster)


# ------------------------------
# Main logic

if __name__ == '__main__':
    print('Constructing PyKineticConn')

    ip_addr = (
        sys.argv[1]
        if   len(sys.argv) > 1
        else '10.23.89.5'
    )
    print(f'Using ip address: {ip_addr}')

    # >> Connect to kinetic
    # NOTE: only `CompareCentroids` uses kconn instance
    kconn = PyKineticConn()
    kconn.Connect(ip_addr, '8123', 'asdfasdf')

    if not kconn.IsConnected():
        err_msg = f'Unable to connect to {ip_addr}'
        print(err_msg)
        sys.exit(err_msg)

    # print('Successful connection!')

    # >> Do comparison
    # if comparing cluster centroids
    # table_diffexpr = CompareCentroids()

    # if comparing cluster cells
    table_diffexpr = CompareCells()

    WritePartition(
         'test/t-statistic'
        ,table_diffexpr
        ,1024 ** 2
    )

    sys.stdout.write(f'[{datetime.datetime.now()}] Diff expr results:\n')
    print(table_diffexpr.to_pandas())

    PrintDataStats()
