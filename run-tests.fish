set run_count 10
set qdepths   2 4 8 16 32

for qdepth in $qdepths
	for run_id in (seq 1 $run_count)
        echo "$run_id |> $qdepth"
        ./build/t.test -o "results/tstat.$qdepth" -d "eval-ebi" -q "$qdepth"
    end
end
