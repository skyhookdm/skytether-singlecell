#!/bin/bash


# ------------------------------
# Global environment

source "${HOME}/.bashrc"

# if pyenv is available, initialize it for this environment
which pyenv >/dev/null
if [[ $? -eq 0 ]]; then
    eval "$(pyenv init --path)"
    eval "$(pyenv virtualenv-init -)"
fi


# ------------------------------
# Global variables

dirpath_workspace="/workspace"
dirpath_skytether="${dirpath_workspace}/code/skytether-singlecell"

build_script="scripts/bootstrap-build.bash"


# ------------------------------
# Main logic

case ${1} in
    build)
        pushd "${dirpath_skytether}"
        poetry run bash "${build_script}"
        poetry run python "src/python/validate.py"
        popd
        ;;

    test)
        pushd "${dirpath_skytether}"
        poetry run python "src/python/validate.py"
        popd
        ;;

    ls)
        ls
        ls code
        ls /opt
        pyenv global
        ;;

    *)
        exec "$@"
        ;;
esac
