#!/bin/bash


# ------------------------------
# Global variables
URI_PYENV="https://raw.githubusercontent.com/pyenv/pyenv-installer/master/bin/pyenv-installer"
URI_POETRY="https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py"

PYTHON_VER="3.9.7"

# >> paths
# where to point a symlink
bin_pyenv="${HOME}/.pyenv/bin/pyenv"
bin_poetry="${HOME}/.poetry/bin/poetry"

# where to place the symlink
link_srcdir="/usr/local/bin"


# ------------------------------
# Tool installations

# >> Install pyenv
which pyenv >/dev/null
if [[ $? -ne 0 ]]; then

    # >> install pyenv itself
    echo "installing pyenv..."
    curl -s -S -L "${URI_PYENV}" | ${SHELL}

    # symlink to binary
    ln -s "${bin_pyenv}" "${link_srcdir}"

    # initialize pyenv
    eval "$(${bin_pyenv} init --path)"
    eval "$(${bin_pyenv} virtualenv-init -)"

else
    echo "pyenv already installed"

fi

# >> Install python (must be 3.9.x)
${bin_pyenv} update

# -s flag skips if the version is already installed
${bin_pyenv} install -s "${PYTHON_VER}"
${bin_pyenv} global "${PYTHON_VER}"


# >> Install poetry
which poetry >/dev/null
if [[ $? -ne 0 ]]; then
    echo "installing poetry..."
    curl -sSL "${URI_POETRY}" | python -

    # symlink to binary
    ln -s "${bin_poetry}" "${link_srcdir}"

else
    echo "poetry already installed"

fi
