#!/bin/bash


# ------------------------------
# Global variables

code_rootdir="${HOME}/code"

# skytether repo contains data management code
repodir_skytether="skytether-singlecell"
repouri_skytether="https://gitlab.com/skyhookdm/skytether-singlecell.git"

# skyhooksctation contains domain-specific code (R and data resources)
repodir_skyhooksctation="skyhooksctation"
repouri_skyhooksctation="https://gitlab.com/skyhookdm/skyhooksctation.git"


# ------------------------------
# create directory for repos

mkdir -p "${code_rootdir}"


# ------------------------------
# checkout repositories

pushd "${code_rootdir}"

# 	|> skytether-singlecell repository
if [[ ! -d "${repodir_skytether}" ]]; then
	git clone "${repouri_skytether}" "${repodir_skytether}"

else
	abspath_skytether="$(readlink -e "${repodir_skytether}")"
	echo -e "skytether checked out:\n\t${abspath_skytether}"

fi

# 	|> skyhooksctation repository
if [[ ! -d "${repodir_skyhooksctation}" ]]; then
	git clone "${repouri_skyhooksctation}" "${repodir_skyhooksctation}"

else
	abspath_sctation="$(readlink -e "${repodir_skyhooksctation}")"
	echo -e "skyhooksctation checked out:\n\t${abspath_sctation}"

fi
