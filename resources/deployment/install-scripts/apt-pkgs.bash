#/bin/bash


# ------------------------------
# Validation

# >> Double check that we're on ubuntu, so that everything else can be specialized
uname -v | grep "Ubuntu" >/dev/null
os_isubuntu=$?

if [[ ${os_isubuntu} -eq 1 ]]; then
    echo "Expected platform to be 'Ubuntu'. Please double-check output of uname"
    exit
fi


# ------------------------------
# Package installation

sudo apt-get update

# dependencies commonly recommended or required
sudo apt-get install -y -V build-essential  \
                           libreadline-dev  \
                           wget

# dependencies recommended for pyenv to install various python versions (Ubuntu)
sudo apt-get install -y -V make             \
                           curl             \
                           libncursesw5-dev \
                           tk-dev           \
                           libxml2-dev      \
                           libxmlsec1-dev   \
                           libsqlite3-dev   \
                           llvm             \
                           libffi-dev       \
                           xz-utils         \
                           zlib1g-dev       \
                           libbz2-dev       \
                           liblzma-dev

# dependencies to install latest arrow packages (via .deb file)
sudo apt-get install -y -V ca-certificates \
                           lsb-release


# dependencies required for kinetic
sudo apt-get install -y -V automake           \
                           autoconf           \
                           libtool            \
                           pkg-config         \
                           libssl-dev         \
                           libprotobuf-dev    \
                           libprotoc-dev      \
                           protobuf-compiler

# dependencies required for skytether
# NOTE: boost filesystem will no longer be needed if we go to a newer version of ubuntu
#       (or if we at least use `-lstdc++fs` ?)
sudo apt-get install -y -V libboost-filesystem-dev \
                           cmake                   \
                           ninja-build
