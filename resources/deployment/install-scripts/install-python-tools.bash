#!/bin/bash


# ------------------------------
# Global variables

# forces script to exit if any command returns non-zero status
# set -e

PYVERS_SKYTETHER="3.9.7"

installuri_pyenv="https://raw.githubusercontent.com/pyenv/pyenv-installer/master/bin/pyenv-installer"
installuri_poetry="https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py"

# paths to binaries we want to symlink
link_dest_pyenv="${HOME}/.pyenv/bin/pyenv"
link_dest_poetry="${HOME}/.poetry/bin/poetry"

# the directory to place a symlink instead of modifying PATH
link_srcdir="/usr/local/bin"

# ------------------------------
# Tool installations

# >> Install pyenv
which pyenv >/dev/null
if [[ $? -ne 0 ]]; then

    # >> install pyenv itself
    echo "installing pyenv..."
    curl -s -S -L "${installuri_pyenv}" | bash

    # symlink to binary
    sudo ln -s "${link_dest_pyenv}" "${link_srcdir}"

    # refresh path changes
    exec "${SHELL}"

else
    echo "pyenv already installed"

fi


# >> Install python (must be 3.9.x)
pyenv update

# -s flag skips if the version is already installed
# --enable-shared tells pyenv (or python) to also build a .so
env PYTHON_CONFIGURE_OPTS="--enable-shared" \
    pyenv install -s "${PYVERS_SKYTETHER}"

pyenv global "${PYVERS_SKYTETHER}"


# >> Install poetry
which poetry >/dev/null
if [[ $? -ne 0 ]]; then
    echo "installing poetry..."
    curl -sSL "${installuri_poetry}" | python -

    # symlink to binary
    sudo ln -s "${link_dest_poetry}" "${link_srcdir}"

else
    echo "poetry already installed"

fi
