#!/bin/bash


# ------------------------------
# Global variables

resourceuri_arrow="https://apache.jfrog.io/artifactory/arrow"

# >> grab system name so we can find the appropriate package info
# lsb_release seems to be debian specific
which lsb_release >/dev/null
if [[ $? -eq 0 ]]; then
    distribution_name="$(lsb_release --id --short | tr 'A-Z' 'a-z')"
    distribution_ver="$(lsb_release --codename --short)"

else
    # catch all for non-debian for now
    distribution_name="unknown"
    distribution_ver="unknown"

fi


# >> generate the URI for package info file
deb_filename="apache-arrow-apt-source-latest-${distribution_ver}.deb"
deburi_arrow="${resourceuri_arrow}/${distribution_name}/${deb_filename}"


# ------------------------------
# Installation logic

# >> get/install the deb file for arrow libraries and update our package list
if [[ ! -f "./${deb_filename}" ]]; then
    wget "${deburi_arrow}"
fi

sudo apt-get install -y -V "./${deb_filename}"
sudo apt-get update


# >> install arrow libraries
sudo apt-get install -y -V libarrow-dev         \
                           libarrow-dataset-dev \
                           libarrow-python-dev  \
                           libarrow-flight-dev  \
                           libgandiva-dev
