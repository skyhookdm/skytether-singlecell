# ------------------------------
# Dependencies
library(reticulate)
library(arrow)

# >> Set python interpreter (rely on pyenv and poetry)
use_python(Sys.which('python'), required=TRUE)

# >> Python dependencies (via reticulate)
skytether <- import('skytether')


# ------------------------------
# Main Logic

metadata_keyname  <- 'expression:001518dd'
table_metadata    <- skytether$ValueForKey(metadata_keyname)

partition_keyname <- 'expression:001518dd.0'
partition_data    <- skytether$TableForKey(partition_keyname)

message(table_metadata)
as.data.frame(
    partition_data[
         c(1, 2, 3, 4)
        ,c('gene_name', 'cell_id', 'expr_val')
    ]
)
