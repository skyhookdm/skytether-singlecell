#!/usr/bin/bash


nano_per_second=$(( 1000000000 ))


echo "------------------------------"
echo "------ E-GEOD-100618 ---------"

time_one_start=$(date '+%s%N')
toolbox/etl-mtx --domain "eval-ebi" --dataset "E-GEOD-100618"
time_one_end=$(date '+%s%N')


echo "------------------------------"
echo "------ E-GEOD-106540 ---------"

time_two_start=$(date '+%s%N')
toolbox/etl-mtx --domain "eval-ebi" --dataset "E-GEOD-106540"
time_two_end=$(date '+%s%N')


echo "------------------------------"
echo "------  E-GEOD-76312  --------"

time_three_start=$(date '+%s%N')
toolbox/etl-mtx --domain "eval-ebi" --dataset "E-GEOD-76312"
time_three_end=$(date '+%s%N')


nano_elapsed_one=$(( ${time_one_end} - ${time_one_start} ))
echo "[E-GEOD-100618] Time (ns): ${nano_elapsed_one}"
echo "[E-GEOD-100618] Time ( s): $(( ${nano_elapsed_one} / ${nano_per_second} ))"

nano_elapsed_two=$(( ${time_two_end} - ${time_two_start} ))
echo "[E-GEOD-106540] Time (ns): ${nano_elapsed_two}"
echo "[E-GEOD-106540] Time ( s): $(( ${nano_elapsed_two} / ${nano_per_second} ))"

nano_elapsed_three=$(( ${time_three_end} - ${time_three_start} ))
echo "[E-GEOD-76312] Time (ns): ${nano_elapsed_three}"
echo "[E-GEOD-76312] Time ( s): $(( ${nano_elapsed_three} / ${nano_per_second} ))"
