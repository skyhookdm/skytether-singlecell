# Skytether -- Single Cell

A domain-specific extension of SkyhookDM for single-cell RNA sequencing. This repository houses
code that is meant to serve as an interface to SkyhookDM for domain-specific logic for manipulating
and processing queries of single-cell gene expression data.


## Languages

The core of the interface to SkyhookDM is written in C++, with python bindings written in Cython.
A python module, `skyteth_r`, is meant to serve as an interface for R code. Another repository,
[skyhooksctation][repo-sctation], houses code and resources to use the `skyteth_r` module via the
[reticulate][docs-reticulate] package.

All source code is in the `src` directory, except for some utility scripts in the `scripts`
directory.


#### C++

The directory, `src/cpp`, contains the core code for this project, `skytether-singlecell`.

In addition to a relatively direct interface to SkyhookDM, there is a prototype C++ implementation
of a SQL interface to kinetic. While it does not contain any canonical skyhook code, it is written
with skyhook in mind and references it for some code snippets (citations to be added).


#### Cython

The directory, `src/python`, contains python code and cython wrappers for the C++ code. Cython code
is located in `src/python/skytether/cybindings` and produces the library, `skytether.scytether`.
The similarity between `skytether` and `scytether` can be confusing at a glance, by the idea is
that s**cy**tether contains **cy**thon bindings for skytether.


## Installation


### Overview

Installation of skytether can broadly be understood as:
1. Install the needed packages ([apt][script-apt-pkgs] or pacman)
2. Install [Apache Arrow C++ libraries][docs-arrowinstall] (via packages or from source)
3. Install python dependencies (development uses [pyenv][docs-pyenv] and [poetry][tool-poetry])
4. Compile skytether C++ with [meson][tool-meson] (depends on `libkinetic`)
5. Compile skytether python


### Using Docker

There is a [Dockerfile][script-docker] (and relevant build context) to make building easier to do, and easier to
understand.

Here are example commands to build an image and run a container using the mentioned `Dockerfile`:

```bash
# tested from `resources/deployment` directory, but should work from any directory
docker build -t xhca/skytether:0.1.0-bionic \
             dockerfiles/ubuntu-bionic

# bind-mount libkinetic installation since it's not a totally open repo
install_prefix_kinetic="/opt/libkinetic"
docker run -it --rm                                                                                \
           --mount type=bind,source="${install_prefix_kinetic}",target="${install_prefix_kinetic}" \
           xhca/skytether:0.1.0-bionic                                                             \
           build
```

Note that for these example commands, the build step prepares the image with everything before
compilation. The run step provides a command, "build", that is passed to the `entrypoint.bash`
script to run the C++ compilation (using `meson`) and, for now, run a "validate.py" script in
`src/python`. This script is only going to work if there is data in the kinetic drive with the
corresponding key names **and** the kinetic drive has the address `10.23.89.5`. Otherwise, an
appropriate error message will appear. There is currently no straight-forward way to improve this
for now, but the easiest way will likely be to add `ARG`s to the `Dockerfile` for the kinetic host
and some way to parameterize the test script.


### Using VirtualBox

#### Networking overview

The easiest way to make VMs communicate with each other *and* the host OS is to use host-only
networking.

Resources:
* [Host-Only Networking][docs-virtualbox-hostonlyif] -- An overview of host-only networking
* [VBoxManage - hostonlyif][docs-vboxmanage-hostonlyif] -- Brief description of hostonlyif sub-command
  of `vboxmanage`
* [Vagrant - Host-Only Networks][docs-vagrant-hostonlyif] -- Brief overview of host-only networks
  with vagrant

##### Examples

###### IP Configuration

My personal setup is as follows.

The subnet is `10.23.89.0/24`.

The `adapter` tab of the network properties (described in `Application interface` section below)
specifies the host's address:

* IPv4 address    : `10.23.89.1`
* IPv4 subnet mask: `255.255.255.0` (this is the default)

The `DHCP Server` tab of the network properties:
* Server **is enabled**
* server address     : `10.23.89.2`
* server mask        : `255.255.255.0`
* lower address bound: `10.23.89.3`
* upper address bound: `10.23.89.254`

###### Application Interface

From the virtualbox GUI, you can do the following:
1. Click `File` in the menu.
2. Select `Host Network Manager`.
3. In the `Network` window that opens:
   1. Create a new network if none exists (or if a new one is desired). Note: networks have the
      name `vboxnet` with a numerical suffix (`0` - `9`), and these names cannot be changed.
   2. Edit properties of the new network -- either by right-clicking on the network in the list, or
      selecting it and pressing the `properties` button just below the menu.
   3. Either use the default IP information, or customize the IP information.

###### Command-line

To create a host-only network via command-line:
1. `sudo vboxmanage hostonlyif create`

To customize host-only network settings via command-line:
1. `sudo vboxmanage hostonlyif ipconfig <network-name> --ip 10.23.89.0`

The network name will be `vboxnet` and some number, e.g. `vboxnet0`. The `--ip` option specifies an
IPv4 subnet, and the default mask is `255.255.255.0` (aka `/24`). To also customize the subnet
mask, it can be provided with the `--netmask` option, e.g. `--netmask 255.255.0.0` for a `/16`
subnet mask. For (slightly more) details, here is the help usage for the `hostonlyif` sub-command:

```bash
>>  sudo vboxmanage hostonlyif

Usage:

VBoxManage hostonlyif       ipconfig <name>
                            [--dhcp |
                            --ip<ipv4> [--netmask<ipv4> (def: 255.255.255.0)] |
                            --ipv6<ipv6> [--netmasklengthv6<length> (def: 64)]]
                            create |
                            remove <name>
```


### Installation Details

Compilation can be tricky due to several aspects: dependency on `libkinetic`, C++ code must be
compiled into libraries for cython code to link against them, and python code has several
dependencies (including the meson build system used for C++).

To summarize a few high-level points:

- `Libkinetic` is implemented in C and must be independently compiled and installed via `make`. A way
  to make `libkinetic` and skytether libraries easily visible to the system is to use `ldconfig`.

- Skytether has a C++ core, but is compiled using `meson`. For some systems, such as Ubuntu 18.04
  (bionic), it is easier to install `meson` via python package manager rather than system packages
  (due to version differences).

- Skytether's cython bindings additionally depend on pyarrow (Apache Arrow's cython bindings).

- Apache Arrow has a C++ core, which can be compiled using `cmake`. It's possible to install
  packages instead of compiling by hand, but for some systems the pyarrow package must be
  re-compiled.

- It is recommended practice with python that virtual environments be used. While this isn't
  necessary during deployment, it is simpler to use the same tooling in development and deployment.
  Thus, poetry is used for python dependency management, but if code uses the python interpreter
  for interfacing with python code (such as using `skyteth_r` from R), it is **important** that the
  correct python interpreter be used.


#### Initial dependencies (Ubuntu packages)

There is a script ([apt-pkgs.bash][script-apt-pkgs] that describes the full apt package list needed, but we also list it here for
convenience.

Dependencies required by multiple tools:
* build-essential
* libreadline-dev
* wget
* pkg-config
* ninja-build

Dependencies required (recommended) by [pyenv][docs-pyenv] to install various versions of python
and supported functionality:
* make
* curl
* llvm
* libffi-dev
* xz-utils
* zlib1g-dev
* libbz2-dev
* liblzma-dev
* _libsqlite3-dev_
* _libncursesw5-dev_
* _tk-dev_
* _libxml2-dev_
* _libxmlsec-1-dev_

Of the above dependencies, there are a few that are _recommended_ that can experimentally be
omitted. I have not put in the effort to figure out which dependencies are absolutely required, but
the last 5 (from _libsqlite3-dev_ to the end) are dependencies that I think are **least needed**.

Dependencies recommended for [installing arrow packages][docs-arrowinstall]:
* ca-certificates
* lsb-release

Dependencies required to compile libkinetic (not necessary if libkinetic binaries and headers are
provided):
* automake
* autoconf
* libtool
* libssl-dev
* libprotobuf-dev
* libprotoc-dev
* protobuf-compiler

Dependencies required by systems that do not provide [std::filesystem][lib-std-fs]:
* libboost-filesystem-dev
* cmake

NOTE: a very simple test to see if `std::filesystem` is provided, is to try compiling the included
`has_filesystem.cpp` source file with the following:

```bash
    g++ -std=c++17 has_filesystem.cpp -lstdc++fs 
```

where `has_filesystem.cpp` is:

```cpp
#include <filesystem>

namespace fs = std::filesystem;

int main()
{
    fs::path aPath {"../"};
    return 0;
}
```

The usage of c++17 as the compilation standard is arbitrary, so feel free to try and use a lower
standard (I think boost would be required, as only c++17 and above provides the std::filesystem
namespace).


#### Apache Arrow C++ libraries

**Install From Packages.** Community provided instructions can be found on the
[arrow install page][docs-arrowinstall]. Otherwise, there is also a bash script,
[install-arrow.bash][script-arrow-pkgs] that programmatically shows how to install Apache Arrow
from apt packages. The gist is to download a `.deb` file, install it with `apt-get install`,
update package repositories, and then install the desired arrow libraries:

```bash
deb_baseuri="https://apache.jfrog.io/artifactory/arrow"
deb_filename="apache-arrow-apt-source-latest-bionic.deb"

wget "${deb_baseuri}/ubuntu/${deb_filename}"
apt-get install -y "./${deb_filename}"
apt-get update
apt-get install -y libarrow-dev         \
                   libarrow-dataset-dev \
                   libarrow-python-dev  \
                   libarrow-flight-dev  \
                   libgandiva-dev
```

**Compile From Source.** I originally built the arrow libraries from source for experience, but
it is much easier to use the provided system packages. However, If you still would like to
compile the arrow libraries from source, here are the commands and options I would use:

```bash
    # This assumes we created a build directory in the "cpp" directory of the arrow repo
    PATH_TO_ARROW_CPP_SOURCE=".."

    cmake -DARROW_COMPUTE=ON         \
          -DARROW_GANDIVA=ON         \
          -DARROW_DATASET=ON         \
          -DARROW_FLIGHT=ON          \
          -DARROW_PARQUET=ON         \
          -DARROW_CSV=ON             \
          -DARROW_FILESYSTEM=ON      \
          -DARROW_PYTHON=ON          \
          -DCMAKE_BUILD_TYPE=Release \
          ${PATH_TO_ARROW_CPP_SOURCE}

    make

    # may have to be sudo depending on install prefix
    make install
```


#### Python dependencies

**Python Dependencies.** I use [poetry][tool-poetry] to manage python dependencies for skytether.
With poetry installed and an appropriate python version set up (`3.9.x`), the installation of
python dependencies is straight-forward. From the root directory (where `pyproject.toml` is
located), run:

```bash
poetry install
```

There is a bash script, [install-python-tools.bash][script-pytools], that helps to install
[pyenv][docs-pyenv], python `3.9.7`, and [poetry][tool-poetry]. These 3 tools are needed to run
the above command without error. After the python dependencies are installed, [meson][tool-meson]
will be available to compile the C++ code. Also, if the Apace Arrow C++ libraries are already
installed, then the pyarrow package can be re-compiled.

**Re-compile pyarrow.** If on a system with an older GCC version (I don't know the particular
versions affected), the pyarrow package must be re-compiled from source. This solution addresses
the scenario where running python code that uses `pyarrow` or `skytether-singlecell` produces an
error that mentions unknown symbols in compiled binaries. For an older system, such as
Ubuntu 18.04 (bionic), there is a bash script, [recompile-pyarrow.bash][script-pyarrow], that
programmatically shows the command to re-compile pyarrow. For convenience, it is also provided
here:

```bash
    # This is the default install prefix
    ARROW_CPP_PREFIX="/usr/lib/x86_64-linux-gnu/cmake/arrow"

    export PYARROW_CMAKE_OPTIONS="-DArrowPython_DIR=${ARROW_CPP_PREFIX}"
    
    python3 -m pip install --no-binary :all: pyarrow
```

Note that the `PYARROW_CMAKE_OPTIONS` environment variable is used by pyarrow's `setup.py` file to
provide options to cmake. The option we want to propagate, `-DArrowPython_DIR`, is used to tell
cmake where to find the compiled C++ bindings for python (defined in Arrow's C++ core and used by
pyarrow). This is only necessary if cmake says that it can't find `ArrowPython`, and can be
referenced if cmake cannot find other installed libraries. If this environment variable is
provided, the C++ install prefix (default: `/usr/lib/x86_64-linux-gnu` on Ubuntu) should be used.
The `cmake/arrow` suffix should be correct for most installations (but may be customized in a
similar way as the C++ install prefix).


#### Compilation - Skytether C++ core

The build system used for the C++ core is the [meson build system][tool-meson]. For simplicity,
meson is defined as a python dependency (in `pyproject.toml`), so it should be installed along
with all other python dependencies.

The build file is named [meson.build][script-meson] and it should be located in the root of the
repository. Meson uses python-like syntax that is (hopefully) readable. The build file has
comments to annotate major sections:
* Project definitions -- This defines anything particular to the project
* Dependencies -- This defines locations to find dependencies (libkinetic), places to install
  artifacts (skytether-singlecell), and variables to gather dependencies and includes into
* Source Lists -- This defines lists of source files to be used for creating libraries and
  executables
* Headers -- This defines headers of interest and where to install them
* Libraries -- This defines libraries to create and how to build them
* Executables -- This defines executables (binaries) to create and how to build them

Note: I tend to bounce between a few different systems, so I have defined sub-sections in the
build file under `Dependencies` that deal with these. The main types of systems are:
1. Archlinux
2. MacOSX
3. Ubuntu
   1. with older C++ packages: uses boost::filesystem
   2. with newer C++ packages: uses std::filesystem


The basic commands to configure the build, compile, and install can be found in the bash script,
[bootstrap-build.bash][script-build]. Here they are, for convenience:

```bash
    build_dirpath="build"

    # set libdir explicitly, otherwise ubuntu defaults to `lib/x86_64-linux-gnu`
    meson --prefix=/opt/skytether-singlecell \
          --libdir=lib                       \
          "${build_dirpath}"

    # cd to build directory, then compile
    meson compile -C "${build_dirpath}"

    # cd to build directory, then install any targets with "install: true"
    meson install -C "${build_dirpath}"
```

Note that for meson, the build directory contains configuration such as installation prefix, etc.
For simplicity, we set this immediately. For compilation and installation, the `-C` flag for the
specifies what directory to use for context. In the above example, the context (build directory)
is "build", so it is equivalent to invoking `cd build` then running `ninja compile` or `ninja
install`.

If you want to change the configuration of the build directory, the `configure` command shows the
existing configuration, and also takes options to change the existing configuration.


#### Compilation - Skytether python library

To compile the cython bindings and install the python library, it is necessary to run `setup.py`
(called `build.cython` in a previous commit). In `setup.py`, paths to compiled libraries (e.g.
shared libraries, or `.so` files) and the skytether C++ code sources must be correct. Currently,
only the `src/storage/kinetic.cpp` source is required. Generally, however, this means that either
the C++ sources must be vendored, or the path to the source repo must be correct (this is the
current approach).

Assuming poetry is being used and its virtualenv is **not activated**, here is the command to
compile and install the skytether package:
```bash
poetry run python setup.py build_ext install
```

If the poetry virtualenv **is activated**, here is the command to compile and install the
skytether package:
```bash
# to activate the poetry virtualenv
poetry shell

...

# to compile and install
python setup.py build_ext install
```

The bash script, [bootstrap-build.bash][script-build], has this command at the end, and assumes
that either:
* `poetry shell` was executed before invoking the build script, or
* the build script was invoked with `poetry run bash bootstrap-build.bash`

The `build_ext` target should compile cython and construct the
`build/lib.linux-x86_64-3.9/scytether.cpython-39-x86_64-linux-gnu.so` shared library that is the
cython code built as a cpython extension. Note that pretty much all of the `setup.py` file is
defined for the cython build. There are probably things I should add to make it a proper python
library, but for now this should be fine.

Finally, the `install` target should install the library wherever your python interpreter stores
such things. The benefit of using [poetry][tool-poetry] is that it is explicit which python
interpreter is being used and it is isolated from the state of your system python (see
[python virtual environments][docs-pyvenv]).

If you are going to use R and [reticulate][docs-reticulate], then be sure to run this command to
install the skytether library for whatever python interpreter you will be using from R. As a
clarifying example: I have a virtual env for developing skytether that is **separate** from the
virtual env I use for running R and reticulate. So, I typically have to install the skytether
package into both environments. This involves activating one virtual env, `cd`ing to my skytether
repository (python sub-dir), running the above install command; and then **repeating** for the
other virtual env.


#### Private Network Setup

When experimenting with the kinetic device, it is useful to have a private network setup. The
network itself only requires a `DHCP server`, as the device should be configured with a `DHCP
client`.

Here is a sample configuration for a DHCP server, using [ISC DHCP][dhcp-iscdhcpd] to lease
addresses for a 10.23.2.0/24 network with 10.23.2.1 being the "router" and using
[Quad9][dns-quad9] for DNS servers:
```
option domain-name-servers 9.9.9.9,149.112.112.112;
option subnet-mask 255.255.255.0;
option routers 10.23.2.1;
subnet 10.23.2.0 netmask 255.255.255.0 {
    range 10.23.2.2 10.23.2.8;
}
```

The configuration above is for a fairly large network (`/24`) but it only leases 7 addresses (from
2 to 8). This is mostly because a `/24` subnet is mentally convenient.

If a linux machine is being used as the router, then it's important that the interface be given
the appropriate address:
```bash
ip addr add 10.23.2.0/24 dev <internal-interface-name>
```

Also, it can be useful to setup the machine so that it propagates internet traffic from the
"external" interface (that can access the internet) to the "internal" interface (that connects to
the private network):
```bash
# As root; required to forward IP packets
echo 1 > /proc/sys/net/ipv4/ip_forward

# does NAT (network-address-translation) from private network to external network
iptables -t nat -A POSTROUTING -s 10.23.2.0/24 -o enp5s0 -j MASQUERADE

# propagates any packets for connections initiated internally
# can use "-i <external-interface-name>" and "-o <internal-interface-name>"
iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT

# propagates any packets sent by the internal network
# can use "-o <external-interface-name>" to be more restrictive
iptables -A FORWARD -i <internal-interface-name> -j ACCEPT
```

There are potentially other iptable rules that can be added regarding icmp, udp, or tcp packets,
but I haven't found them to be strictly required, so I'm not sure in which cases they may be
useful.

Finally, be sure that the `/etc/resolv.conf` file on the kinetic device specifies appropriate DNS
servers:
```
# DNS for Quad9
nameserver 9.9.9.9
nameserver 149.112.112.112
```


## Metadata Notes

### Single cell metadata for Stuart Lab

Notation for metadata format:
- Metadata attributes (aka categories):
  - lab                 : group who generated the data
  - platform            : scRNA-Seq platform used
  - species             : species name in the format of, e.g. "hsapiens"
  - batch               : ID indicating biological replicates
  - tissue              : tissue origin of cells
  - developmental-stage : e.g. adult or E9.5
  - cell-cluster        : cell type or cluster ID if cell type annotation not available

- "\_" A delimiter to separate attributes of a metadata string

- "+" A delimiter to separate required values from optional values within a metadata attribute


Here is a simple example that only provides a single value for each required attribute:

    stuart_10x_hsapiens_replicate-1_lung_adult_t-cell


Here is a simple example that provides a single value for each required attribute; an optional
value for the `cell-cluster` attribute ("cd1-strain"); and an extra value, "tamoxifen-treated", for
an unspecified attribute (maybe "treatment"):

    stuart_10x_mmusculus_sample-2_lung_E9.5_cluster-1+cd1-strain_tamoxifen-treated


<!-- resources -->
[lib-std-fs]:        https://en.cppreference.com/w/cpp/filesystem
[tool-meson]:        https://mesonbuild.com/
[tool-poetry]:       https://python-poetry.org/

[docs-arrowinstall]: https://arrow.apache.org/install/
[docs-pyvenv]:       https://docs.python.org/3/tutorial/venv.html
[docs-reticulate]:   https://rstudio.github.io/reticulate/

[repo-sctation]:     https://gitlab.com/skyhookdm/skyhooksctation/

[script-apt-pkgs]:   https://gitlab.com/skyhookdm/skytether-singlecell/-/blob/9f91b818ce8df56f2dee36a7098f3af815b7341d/resources/deployment/install-scripts/apt-pkgs.bash
[script-arrow-pkgs]: https://gitlab.com/skyhookdm/skytether-singlecell/-/blob/9f91b818ce8df56f2dee36a7098f3af815b7341d/resources/deployment/install-scripts/install-arrow.bash
[script-pyarrow]:    https://gitlab.com/skyhookdm/skytether-singlecell/-/blob/9f91b818ce8df56f2dee36a7098f3af815b7341d/scripts/recompile-pyarrow.bash
[script-pytools]:    https://gitlab.com/skyhookdm/skytether-singlecell/-/blob/9f91b818ce8df56f2dee36a7098f3af815b7341d/resources/deployment/dockerfiles/ubuntu-bionic/install-python-tools.bash
[script-build]:      https://gitlab.com/skyhookdm/skytether-singlecell/-/blob/9f91b818ce8df56f2dee36a7098f3af815b7341d/scripts/bootstrap-build.bash
[script-meson]:      https://gitlab.com/skyhookdm/skytether-singlecell/-/blob/9f91b818ce8df56f2dee36a7098f3af815b7341d/meson.build
[script-docker]:     https://gitlab.com/skyhookdm/skytether-singlecell/-/blob/b88855879b9e5a47d90d2e0581d199c4012125ee/resources/deployment/dockerfiles/ubuntu-bionic/Dockerfile

<!-- networking -->
[dns-quad9]:         https://www.quad9.net/
[dhcp-iscdhcpd]:     https://kb.isc.org/docs/isc-dhcp-44-manual-pages-dhcpd



[docs-virtualbox-hostonlyif]: https://www.virtualbox.org/manual/UserManual.html#network_hostonly
[docs-vboxmanage-hostonlyif]: https://www.virtualbox.org/manual/UserManual.html#vboxmanage-hostonlyif
[docs-vagrant-hostonlyif]:    https://www.vagrantup.com/docs/providers/virtualbox/networking#virtualbox-host-only-networks
