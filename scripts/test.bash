#!/usr/bin/bash

# rows_per_mb=$(( 25768 ))

num_rows=$(( 1 ))
for mult in $(seq 1 12); do
    # head --lines=${num_rows}    "data/1_1_1_1_1_500000.tsv" > "input-byrows/matrix.${num_rows}.tsv"
    ./build/convert-file-format "data/1_1_1_1_1_500000.tsv"   "geneexpr-byrows/matrix.${num_rows}" ${num_rows}

    num_rows=$(( ${num_rows} * 2 ))
done
