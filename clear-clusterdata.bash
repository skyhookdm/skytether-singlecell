#!/usr/bin/bash

/opt/libkinetic/bin/kctl -h "10.23.89.5" del -S 'eval-ebi.centroids' -e 'eval-ebi.centroids<'
/opt/libkinetic/bin/kctl -h "10.23.89.5" del -S 'eval-ebi.clusters' -e 'eval-ebi.clusters<'

datasets=("E-GEOD-106540" "E-GEOD-100618" "E-GEOD-76312")
for dataset in ${datasets[@]}; do
    echo "removing cell clusters from [${dataset}]"

    # For debugging
    # /opt/libkinetic/bin/kctl -h "10.23.89.5" range -S "eval-ebi/${dataset}.clusters" -e "eval-ebi/${dataset}.clusters<"

    /opt/libkinetic/bin/kctl -h "10.23.89.5" del -S "eval-ebi/${dataset}.clusters" -e "eval-ebi/${dataset}.clusters<"
done
